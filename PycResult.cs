﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PyjReader
{
    public class PycResult
    {
        public List<string> Result = new List<string>();
        private int Indent = 0;

        public void IncreaseIndent()
        {
            ++Indent;
        }

        public void DecreaseIndent()
        {
            Indent--;
            if (Indent < 0)
                Indent = 0;
        }

        public void Clear()
        {
            Indent = 0;
            Result.Clear();
        }

        public void Add(string line)
        {
            string padding = "";

            for (int idx = 0; idx < Indent; idx++)
            {
                padding += "    ";
            }

            Result.Add(padding + line);
        }

        public void Add(List<string> lines)
        {
            string padding = "";

            for (int idx = 0; idx < Indent; idx++)
            {
                padding += "    ";
            }

            foreach (string line in lines)
            {
                Result.Add(padding + line);
            }
        }

        public void Add(PycResult data)
        {
            string padding = "    ";

            foreach (string line in data.Result)
            {
                Result.Add(padding + line);
            }
        }

        public void Replace(string from, string to)
        {
            for (int idx = 0; idx < Result.Count; idx++)
            {
                Result[idx].Replace(from, to);
            }
        }

        public bool HasResult { get { return Result.Count > 0; } }

        public override string ToString()
        {
            return String.Join("\n",Result);
        }
    }
}
