﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Zip.Compression;
using ICSharpCode.SharpZipLib.Zip.Compression.Streams;


namespace PyjReader
{
    class Program
    {
        //const string gamePath = @"C:\Sergey\EVE";
        //const string dataPath = @"C:\Sergey\Decompilers\Blue.dll\eve\";
        const string gamePath = @"D:\Games\EVE";
        const string dataPath = @"D:\Data\EVE";
        const string binPath = dataPath + @"\bin";
        const string scriptPath = dataPath + @"\script";
        const string rootPath = dataPath + @"\root";

        static public IntPtr VerCryptKey = IntPtr.Zero;
        static void Main(string[] args)
        {
            //if (args.Length == 0)
            //{
                DecryptPyjLibs();
//                DecryptCompiledCode();
            //}
            //else
            //{
            //    string fileName = "";
            //    if (args.Length == 0)
            //        fileName = dataPath + @"\lib\carbonlib.ccp\nasty.pyj";
            //    else
            //        fileName = args[0];

            //    DecompileModule(fileName);
            //}
        }

        private static void DecryptCompiledCode()
        {
            List<object> data = Unpickle.Load(gamePath + @"\script\compiled.code");
            List<object> data1 = Unpickle.Load((byte[])data[1]);

            DecryptAndDecompileSource((byte[])data1[2], (byte[])data1[4]);
            int counter = 1;
            int maxCounter = data1.Count - 5;
            for (int idx = 6; idx < maxCounter; )
            {
                counter++;
                Console.Write("{0}/{1}: ", counter, maxCounter );
                DecryptAndDecompileSource((byte[])data1[idx], (byte[])data1[idx + 1]);
                idx += 3;
            }
        }

        private static void DecryptAndDecompileSource(byte[] fileBuffer, byte[] dataBuffer)
        {
            string filename = PycReader.ConvertBytesToString(fileBuffer);
            filename = Path.GetFullPath(filename.Replace("script:", scriptPath).Replace("bin:", binPath).Replace("root:", rootPath).Replace(@"/..", ""));
            if (filename.EndsWith(".py"))
            {
                filename = Path.GetDirectoryName(filename) + @"\" + Path.GetFileNameWithoutExtension(filename) + ".pyc";
            }
            string srcFilename = Path.GetDirectoryName(filename) + @"\" + Path.GetFileNameWithoutExtension(filename) + ".py";
            string asmFilename = Path.GetDirectoryName(filename) + @"\" + Path.GetFileNameWithoutExtension(filename) + ".pyasm";
            Console.WriteLine("Processing file {0}", filename);
            DecryptStringToFile(dataBuffer, filename);

            return;
            try
            {
                PycReader rdr = new PycReader(filename);
                PythonObject obj = rdr.ReadObject();
                string resultAsm = rdr.DumpObject(obj);
                PycResult result = PycDecompiler.Decompile(obj as PythonCodeObject);

                StreamWriter writer = new StreamWriter(srcFilename);
                writer.Write(String.Join("\n",result.Result.ToArray()));
                writer.Close();

                writer = new StreamWriter(asmFilename);
                writer.Write(resultAsm);
                writer.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("EXCEPTION: {0}", ex);
            }

        }

        private static void DecompileModule(string filename)
        {

            return;
            if (filename.EndsWith(".py"))
            {
                filename = Path.GetDirectoryName(filename) + @"\" + Path.GetFileNameWithoutExtension(filename) + ".pyc";
            }
            string srcFilename = Path.GetDirectoryName(filename) + @"\" + Path.GetFileNameWithoutExtension(filename) + ".py";
            string asmFilename = Path.GetDirectoryName(filename) + @"\" + Path.GetFileNameWithoutExtension(filename) + ".pyasm";

            try
            {
                PycReader rdr = new PycReader(filename);
                PythonObject obj = rdr.ReadObject();
                string resultAsm = rdr.DumpObject(obj);
                PycResult result = PycDecompiler.Decompile(obj as PythonCodeObject);

                StreamWriter writer = new StreamWriter(srcFilename);
                writer.Write(String.Join("\n", result.Result.ToArray()));
                writer.Close();

                writer = new StreamWriter(asmFilename);
                writer.Write(resultAsm);
                writer.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("EXCEPTION: {0}", ex);
            }
        }

        public static void DecryptPyjLibs()
        {
            if (VerCryptKey == IntPtr.Zero)
                VerCryptKey = GetVerCryptKey();

            ZipFileReader.ExtractZipFile(gamePath + @"\code.ccp", dataPath + @"\code.ccp\", filename =>
                                                                                                                                {
                                                                                                                                    DecryptFile(filename, VerCryptKey);
                                                                                                                                    DecompileModule(filename);
                                                                                                                                    return true;
                                                                                                                                });

            //ZipFileReader.ExtractZipFile(gamePath + @"\lib\carbonstdlib.ccp", dataPath + @"\lib\carbonstdlib.ccp\", filename =>
            //                                                                                                                    {
            //                                                                                                                        DecryptFile(filename, VerCryptKey);
            //                                                                                                                        DecompileModule(filename);
            //                                                                                                                        return true;
            //                                                                                                                    });
            //ZipFileReader.ExtractZipFile(gamePath + @"\lib\carbonlib.ccp", dataPath + @"\lib\carbonlib.ccp\", filename =>
            //                                                                                                            {
            //                                                                                                                DecryptFile(filename, VerCryptKey);
            //                                                                                                                DecompileModule(filename);
            //                                                                                                                return true;
            //                                                                                                            });
            //ZipFileReader.ExtractZipFile(gamePath + @"\lib\evelib.ccp", dataPath + @"\lib\evelib.ccp\", filename =>
            //                                                                                                        {
            //                                                                                                            DecryptFile(filename, VerCryptKey);
            //                                                                                                            DecompileModule(filename);
            //                                                                                                            return true;
            //                                                                                                        });
        }

        private static long FindFileOffset(BinaryReader reader)
        {
            String keySignatureString = "\x06\x02\0\0\0\x24\0\0RSA1";
            byte[] keySignature = Encoding.ASCII.GetBytes(keySignatureString);
            byte[] data = new byte[reader.BaseStream.Length];
            int idx2 = 0;

            reader.Read(data, 0, (int)reader.BaseStream.Length);
            for (int idx = 0; idx < reader.BaseStream.Length - keySignature.Length + 1; idx++)
            {
                if (data[idx] == keySignature[0])
                {
                    for (idx2 = 0; idx2 < keySignature.Length; idx2++)
                    {
                        if (data[idx + idx2] != keySignature[idx2])
                            break;
                    }

                    if (idx2 == keySignature.Length)
                        return idx;

                }
            }
            return 0;
        }

        private static void DecryptFile(string filename, IntPtr verCryptKey)
        {
            int read;
            Console.WriteLine("Processing file {0}", filename);
            BinaryReader rdr = new BinaryReader(File.OpenRead(filename));
            byte[] data = rdr.ReadBytes((int)rdr.BaseStream.Length);
            rdr.Close();
            uint size = (uint)data.Length;
            Crypt32.CryptDecrypt(verCryptKey, IntPtr.Zero, 1, 0, data, ref size);
            MemoryStream inStream = new MemoryStream(data, 0, (int)size);
            FileStream outStream = File.OpenWrite(filename);
            InflaterInputStream inflater = new InflaterInputStream(inStream);
            byte[] outData = new byte[32*1024];
            read = 0;
            while((read = inflater.Read(outData,0,outData.Length)) > 0)
            {
                outStream.Write(outData,0,read);
            }
            outStream.Close();
        }

        private static void DecryptStringToFile(byte[] data, string filename)
        {
            if (VerCryptKey == IntPtr.Zero)
                VerCryptKey = GetVerCryptKey();

            int read;
            uint size = (uint)data.Length;
            Crypt32.CryptDecrypt(VerCryptKey, IntPtr.Zero, 1, 0, data, ref size);
            MemoryStream inStream = new MemoryStream(data, 0, (int)size);
            if (!Directory.Exists(Path.GetDirectoryName(filename)))
                Directory.CreateDirectory(Path.GetDirectoryName(filename));
            FileStream outStream = File.OpenWrite(filename);
            InflaterInputStream inflater = new InflaterInputStream(inStream);
            byte[] outData = new byte[32 * 1024];
            read = 0;
            while ((read = inflater.Read(outData, 0, outData.Length)) > 0)
            {
                outStream.Write(outData, 0, read);
            }
            outStream.Close();
        }


        public static IntPtr GetVerCryptKey()
        {
            IntPtr phProv = new IntPtr();
            string pszContainer = "";
            byte[] keyData = null;

            bool rc = Crypt32.CryptAcquireContext(ref phProv, pszContainer, "Microsoft Enhanced Cryptographic Provider v1.0", 1, 0xF0000000);

            BinaryReader reader = new BinaryReader(File.OpenRead(gamePath + @"\bin\blue.dll"));
            reader.BaseStream.Position = FindFileOffset(reader);
            if (reader.BaseStream.Position == 0)
            {
                return IntPtr.Zero;
            }
            byte[] pbData = reader.ReadBytes(0x94);
            reader.BaseStream.Position += 20;
            byte[] pbNextData = reader.ReadBytes(0x8C);
            IntPtr verKey = new IntPtr();
            IntPtr verCryptKey = new IntPtr();
            IntPtr hKey = new IntPtr();
            IntPtr hPubKey = new IntPtr();
            rc = Crypt32.CryptImportKey(phProv, pbData, 0x94, IntPtr.Zero, 0, ref verKey);
            rc = Crypt32.CryptGenKey(phProv, 1, 1, ref hKey);
            int dataLen = 0;
            rc = Crypt32.CryptExportKey(hKey, IntPtr.Zero, 7, 0, null, ref dataLen);
            if (dataLen > 0)
            {
                keyData = new byte[dataLen];
                rc = Crypt32.CryptExportKey(hKey, IntPtr.Zero, 7, 0, keyData, ref dataLen);
                Crypt32.PRIVATE_KEY_BLOB privateKeyBlob = Crypt32.CopyByteArrayToPrivateKeyBlob(keyData);
                privateKeyBlob.rsaPubKey.pubexp = 1;

                Array.Clear(privateKeyBlob.exponent1, 0, privateKeyBlob.exponent1.Length);
                privateKeyBlob.exponent1[0] = 1;

                Array.Clear(privateKeyBlob.exponent2, 0, privateKeyBlob.exponent2.Length);
                privateKeyBlob.exponent2[0] = 1;

                Array.Clear(privateKeyBlob.privateExponent, 0, privateKeyBlob.privateExponent.Length);
                privateKeyBlob.privateExponent[0] = 1;

                keyData = Crypt32.CopyPrivateKeyBlobToByteArray(privateKeyBlob);
            }
            rc = Crypt32.CryptDestroyKey(hKey);

            rc = Crypt32.CryptImportKey(phProv, keyData, keyData.Length, IntPtr.Zero, 0, ref hPubKey);

            rc = Crypt32.CryptImportKey(phProv, pbNextData, 0x8C, hPubKey, 0, ref verCryptKey);
            rc = Crypt32.CryptDestroyKey(hPubKey);
            reader.Close();

            return verCryptKey;
        }
    }

}
