﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PyjReader
{
    public class ASTNode : PythonObject
    {
        public ASTNode Parent { get; set; }
        public OpCode Command { get; set; }
        public Dictionary<string, PythonObject> Children { get; set; }
        public string Action { get; set; }

        public ASTNode()
        {
            ClassName = "ASTNode";
            Children = new Dictionary<string, PythonObject>();
        }
    }
}
