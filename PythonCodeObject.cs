﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PyjReader
{
    public class PythonCodeObject : PythonObject
    {
        public UInt32 ArgCount { get; set; }
        public UInt32 NumLocals { get; set; }
        public UInt32 StackSize { get; set; }
        public UInt32 Flags { get; set; }
        public byte[] Code { get; set; }
        public List<PythonObject> Consts { get; set; }
        public List<PythonObject> Names { get; set; }
        public List<PythonObject> VarNames { get; set; }
        public List<PythonObject> FreeVars { get; set; }
        public List<PythonObject> CellVars { get; set; }
        public string FileName { get; set; }
        public string Name { get; set; }
        public UInt32 FirstLineNo { get; set; }
        public List<int> LineNoTab { get; set; }
        public Dictionary<uint, PythonCodeObject> Methods { get; set; }
        public PycResult SourceCode { get; set; }
        public string FuncParams { get; set; }
        public PycResult FuncDecos { get; set; }
        public string FuncName { get; set; }
        public List<ASTNode> ASTTree { get; set; }
    }
}
