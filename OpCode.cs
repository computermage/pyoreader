﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PyjReader
{
    public class OpCode
    {
        public byte OpCodeID { get; set; }
        public string InstructionName { get; set; }
        public bool HasArgument { get; set; }
        public uint Argument { get; set; }
        public bool HasName { get; set; }
        public string Name { get; set; }
        public bool HasJumpRelative { get; set; }
        public bool HasJumpAbsolute { get; set; }
        public bool HasConstant { get; set; }
        public string Constant { get; set; }
        public bool HasCompare { get; set; }
        public string CompareOperator { get; set; }
        public bool HasLocal { get; set; }
        public string LocalName { get; set; }
        public bool HasFree { get; set; }
        public string FreeName { get; set; }
        public int Offset { get; set; }

        public uint JumpTarget
        {
            get
            {
                if (HasJumpRelative)
                {
                    return (uint)Offset + 3 + Argument;
                }
                else if (HasJumpAbsolute)
                {
                    return Argument;
                }
                else
                    return 0;
            }
        }

        public OpCode(byte opcode, string name)
        {
            OpCodeID = opcode;
            InstructionName = name;

        }

        public OpCode Clone()
        {
            return new OpCode(OpCodeID, InstructionName) { HasArgument = this.HasArgument, Argument = this.Argument, HasName = this.HasName, Name = this.Name,
                                                           HasJumpAbsolute = this.HasJumpAbsolute, HasJumpRelative = this.HasJumpRelative,
                                                           HasConstant = this.HasConstant, Constant = this.Constant, HasCompare = this.HasCompare, CompareOperator = this.CompareOperator,
                                                           HasLocal = this.HasLocal, LocalName = this.LocalName, HasFree = this.HasFree, FreeName = this.FreeName, Offset = this.Offset };
        }
    }
}
