﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace PyjReader
{
    public static class ByteArrayToStructureCopier
    {
        public static T ByteArrayToStructure<T>(this byte[] bytes, int offset = 0) where T : struct
        {
            GCHandle handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);
            T stuff = (T)Marshal.PtrToStructure(handle.AddrOfPinnedObject() + offset,
                typeof(T));
            handle.Free();
            return stuff;
        }

        public static byte[] StructureToByteArray<T>(this T struc) where T : struct
        {
            int size = Marshal.SizeOf(struc);
            byte[] arr = new byte[size];
            IntPtr ptr = Marshal.AllocHGlobal(size);

            Marshal.StructureToPtr(struc, ptr, true);
            Marshal.Copy(ptr, arr, 0, size);
            Marshal.FreeHGlobal(ptr);

            return arr;
        }
    }
}
