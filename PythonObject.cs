﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PyjReader
{
    public class PythonObject
    {
        public string ClassName { get; set; }
        public object Value { get; set; }

        public PythonObject()
        {
        }

        public PythonObject(string class_name)
        {
            ClassName = class_name;
        }

        public void Add(PythonObject po)
        {
            if (new[] { "Py_List", "Py_Tuple", "Py_Set", "Py_FrozenSet" }.Contains(ClassName))
            {
                List<PythonObject> list = Value as List<PythonObject>;

                if (list == null)
                {
                    list = new List<PythonObject>();
                    Value = list;
                }

                list.Add(po);
            }
        }

        public void Add(PythonObject key, PythonObject value)
        {
            if (ClassName == "Py_Dict")
            {
                Dictionary<PythonObject, PythonObject> dict = Value as Dictionary<PythonObject, PythonObject>;
                if (dict == null)
                {
                    dict = new Dictionary<PythonObject, PythonObject>();
                    Value = dict;
                }

                dict.Add(key, value);
            }
        }

        public int Count
        {
            get
            {
                if (new[] { "Py_List", "Py_Tuple", "Py_Set", "Py_FrozenSet" }.Contains(ClassName))
                {
                    List<PythonObject> list = Value as List<PythonObject>;
                    if (list != null)
                    {
                        return list.Count;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else if (ClassName == "Py_Dict")
                {
                    Dictionary<PythonObject, PythonObject> dict = Value as Dictionary<PythonObject, PythonObject>;
                    if (dict != null)
                    {
                        return dict.Count;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else if (ClassName == "Py_VeryLong")
                {
                    List<int> list = Value as List<int>;
                    if (list != null)
                    {
                        return list.Count;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    return 1;
                }
            }
        }

        public static implicit operator string(PythonObject v)
        {
            if (new[] { "Py_Int", "Py_Float", "Py_Long", "Py_Long64" }.Contains(v.ClassName))
            {
                return v.Value.ToString();
            }
            if (new[] { "Py_VeryLong" }.Contains(v.ClassName))
            {
                string result = "0x";
                List<int> list = v.Value as List<int>;
                result += list[list.Count - 1].ToString("X");
                for (int idx = list.Count - 2; idx >= 0; idx--)
                {
                    result += list[idx].ToString("X8");
                }
                return result;
            }
            if (v.ClassName == "Py_Null")
            {
                return "null";
            }
            if (v.ClassName == "Py_False")
            {
                return "false";
            }
            if (v.ClassName == "Py_True")
            {
                return "true";
            }
            if (v.ClassName == "Py_None")
            {
                return "None";
            }

            if (new[] { "Py_Tuple", "Py_Set", "Py_FrozenSet" }.Contains(v.ClassName))
            {
                string res = "(";
                if (v.Value is List<PythonObject>)
                {
                    foreach (PythonObject o in v.Value as List<PythonObject>)
                    {
                        if (res != "(")
                        {
                            res += ", " + o;
                        }
                        else
                        {
                            res += o;
                        }
                    }
                    res += ")";
                    return res;
                }
            }

            if (v.ClassName == "Py_CodeObject")
            {
                PythonCodeObject co = v as PythonCodeObject;

                return String.Format(@"<code object {0}, file '{1}', line {2}>", co.Name, co.FileName, co.FirstLineNo);
            }

            if (v.ClassName == "Py_Dict")
            {
                string res = "(";
                if (v.Value is Dictionary<PythonObject, PythonObject>)
                {
                    foreach (KeyValuePair<PythonObject, PythonObject> pair in v.Value as Dictionary<PythonObject, PythonObject>)
                    {
                        if (res != "(")
                        {
                            res += ", " + pair.Value + ": " + pair.Value;
                        } else
                        {
                            res += pair.Value + ": " + pair.Value;
                        }
                    }
                    res += ")";
                    return res;
                }
            }


            return new[] { "Py_Unicode", "Py_String", "Py_Interned", "Py_StringRef" }.Contains(v.ClassName) ? PycReader.ConvertBytesToString((byte[])v.Value) : null;
        }

        public static implicit operator byte [](PythonObject v)
        {
            return new[] { "Py_Unicode", "Py_String", "Py_Interned", "Py_StringRef" }.Contains(v.ClassName) ? (byte[])v.Value : null;
        }

        public static implicit operator long(PythonObject v)
        {
            return new[] { "Py_Long", "Py_Long64" }.Contains(v.ClassName) ? (long)v.Value : 0;
        }

        public static implicit operator int (PythonObject v)
        {
            return new[] { "Py_Int", "Py_Long", "Py_Long64" }.Contains(v.ClassName) ? (int)v.Value : 0;
        }

        public static implicit operator uint(PythonObject v)
        {
            return new[] { "Py_Int", "Py_Long", "Py_Long64" }.Contains(v.ClassName) ? (uint)v.Value : 0;
        }

        public static implicit operator double(PythonObject v)
        {
            return v.ClassName == "Py_Float" ? (double)v.Value : 0;
        }

        public static implicit operator List<PythonObject>(PythonObject v)
        {
            if (v == null || v.ClassName == null)
                return null;
            return new[] { "Py_Tuple", "Py_Set", "Py_FrozenSet" }.Contains(v.ClassName) ? v.Value as List<PythonObject> : null;
        }
        public static implicit operator Dictionary<PythonObject, PythonObject>(PythonObject v)
        {
            return v.ClassName == "Py_Dict" ? v.Value as Dictionary<PythonObject, PythonObject> : null;
        }

        public static implicit operator PythonObject(uint v)
        {
            return new PythonObject { ClassName = "Py_Int", Value = v };
        }

        public static implicit operator PythonObject(int v)
        {
            return new PythonObject { ClassName = "Py_Int", Value = v };
        }
        public static implicit operator PythonObject(double v)
        {
            return new PythonObject { ClassName = "Py_Float", Value = v };
        }
        public static implicit operator PythonObject(float v)
        {
            return new PythonObject { ClassName = "Py_Float", Value = (double)v };
        }

        public static implicit operator PythonObject(long v)
        {
            return new PythonObject { ClassName = "Py_Long", Value = v };
        }

        public static implicit operator PythonObject(ulong v)
        {
            return new PythonObject { ClassName = "Py_Long", Value = v };
        }

        public static implicit operator PythonObject(string v)
        {
            return new PythonObject { ClassName = "Py_String", Value = Encoding.ASCII.GetBytes(v) };
        }

        public static implicit operator PythonObject(byte[] v)
        {
            return new PythonObject { ClassName = "Py_String", Value = v };
        }
    }
}
