﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PyjReader
{
    public class PycDisassembler
    {
        public static string Disassembler(PythonCodeObject obj)
        {
            string result = "";
            int idx = 0;
            BinaryReader rdr = new BinaryReader(new MemoryStream(obj.Code));
            int extendedArg = 0;
            int currentAddress = 0;
            int lastLine = 0;
            byte opCode = 0;
            OpCode decoder = null;

            while (idx < rdr.BaseStream.Length)
            {
                try
                {
                    currentAddress = idx;
                    if (obj.LineNoTab[currentAddress] != lastLine)
                    {
                        lastLine = obj.LineNoTab[currentAddress];
                        result += String.Format("\r\n{0,-10}", lastLine);
                    } else
                    {
                        result += String.Format("{0,-10}", " ");

                    }
                    opCode = rdr.ReadByte();
                    decoder = OpCodes.OpCodeList[opCode];
                    idx++;
                    int opArg = -1;
                    string opArgValue = "";
                    if (decoder.HasArgument)
                    {
                        opArg = rdr.ReadInt16() + extendedArg;
                        extendedArg = 0;
                        idx += 2;
                        opArgValue = "";
                        if (opCode == OpCodes.EXTENDED_ARG)
                            extendedArg = opArg * 65536;
                        if (decoder.HasJumpRelative)
                            opArgValue = "to " + (opArg + idx).ToString();
                        if (decoder.HasConstant)
                        {
                            PythonObject val = obj.Consts[opArg];
                            opArgValue = val.ClassName == "Py_String" ?  "'" + (string)val + "'" : (string)val;
                        } else if (decoder.HasName)
                        {
                            opArgValue = obj.Names[opArg];
                        } else if (decoder.HasCompare)
                        {
                            opArgValue = "'" + OpCodes.CompareOpNames[opArg] +"'";
                        } else if (decoder.HasLocal)
                        {
                            opArgValue = obj.VarNames[opArg];
                        } else if (decoder.HasFree)
                        {
                            if (opArg < obj.CellVars.Count)
                            {
                                opArgValue = obj.CellVars[opArg];
                            }else if (opArg < obj.FreeVars.Count)
                            {
                                opArgValue = obj.FreeVars[opArg];
                            } else
                            {
                                opArgValue = "ERROR: Cannot calculate Free Var for index " + opArg;
                            }
                        }
                    }
                    result += String.Format("{0} {1} {2} {3}\n", currentAddress, decoder.InstructionName, decoder.HasArgument ? opArg.ToString() : "", opArgValue != "" ? "(" + opArgValue + ")" : "");
                }
                catch(EndOfStreamException)
                {
                    return result;
                }
                catch (Exception ex)
                {
                    result += "EXCEPTION: " + ex.Message;
                }
            }

            return result;
        }



    }
}
