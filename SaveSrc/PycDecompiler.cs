﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace PyjReader
{
    public class PycDecompiler
    {

        public static string Decompile(PythonCodeObject obj, List<string> defaults = null)
        {
            if (obj == null)
                return "";
            List<string> result = new List<string>();
            OpCode opCode = new OpCode(obj);
            int currentAddress = 0;
            OpCodeDecoder decoder = null;
            OpCodeDecoder nextDecoder = null;
            Stack<string> DataStack = new Stack<string>();
            Stack<int> ElseStack = new Stack<int>();

            if (obj.Name != "<module>")
            {
                string pars = "";
                if (obj.ArgCount > 0)
                {
                    for (int i = 0; i < obj.ArgCount; i++)
                    {
                        string param = obj.VarNames[i];
                        if ((i + 1) > (obj.ArgCount - defaults.Count))
                            param += " = " + defaults[i - ((int)obj.ArgCount - defaults.Count)];
                        pars += pars == "" ? param : ", " + param;
                    }

                }
                result.Add(String.Format("def {0} ({1}):", obj.Name, pars));
            }


            while (opCode.HasInstructionsToProcess)
            {
                try
                {
                    decoder = opCode.GetNextInstruction();
                    currentAddress = decoder.Offset;
                    //if (obj.LineNoTab[currentAddress] != lastLine)
                    //{
                    //    lastLine = obj.LineNoTab[currentAddress];
                    //    result.Add(String.Format("\r\n{0,-10}", lastLine);
                    //} else
                    //{
                    //    result.Add(String.Format("{0,-10}", " ");

                    //}
                    string v1 = "", v2 = "", v3 = "", v4 = "";

                    switch (decoder.OpCode)
                    {
                        case OpCode.POP_TOP:
                            result.Add(DataStack.Pop());
                            break;
                        case OpCode.ROT_TWO:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            DataStack.Push(v1);
                            DataStack.Push(v2);
                            break;
                        case OpCode.ROT_THREE:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v3 = DataStack.Pop();
                            DataStack.Push(v1);
                            DataStack.Push(v2);
                            DataStack.Push(v3);
                            break;
                        case OpCode.DUP_TOP:
                            v1 = DataStack.Peek();
                            DataStack.Push(v1);
                            break;
                        case OpCode.ROT_FOUR:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v3 = DataStack.Pop();
                            v4 = DataStack.Pop();
                            DataStack.Push(v1);
                            DataStack.Push(v2);
                            DataStack.Push(v3);
                            DataStack.Push(v4);
                            break;
                        //case OpCode.NOP:
                        //    break;
                        case OpCode.UNARY_POSITIVE:
                            v1 = DataStack.Pop();
                            v1 = v1[0] == '-' ? v1.Substring(1) : v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.UNARY_NEGATIVE:
                            v1 = DataStack.Pop();
                            v1 = v1[0] == '-' ? v1 : v1.Substring(1);
                            DataStack.Push(v1);
                            break;
                        case OpCode.UNARY_NOT:
                            v1 = DataStack.Pop();
                            v1 = "not " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.UNARY_CONVERT:
                            v1 = DataStack.Pop();
                            v1 = "repr " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.UNARY_INVERT:
                            v1 = DataStack.Pop();
                            v1 = "not " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.BINARY_POWER:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " ** " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.BINARY_MULTIPLY:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " * " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.BINARY_DIVIDE:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " / " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.BINARY_MODULO:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " % " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.BINARY_ADD:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " + " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.BINARY_SUBTRACT:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " - " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.BINARY_SUBSCR:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + "[" + v1 + "]";
                            DataStack.Push(v1);
                            break;
                        case OpCode.BINARY_FLOOR_DIVIDE:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " // " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.BINARY_TRUE_DIVIDE:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " / " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.INPLACE_FLOOR_DIVIDE:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + @" //= " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.INPLACE_TRUE_DIVIDE:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " /= " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.SLICE:
                            v1 = DataStack.Pop();
                            v1 = v1 + "[:]";
                            DataStack.Push(v1);
                            break;
                        case OpCode.SLICE1:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + "[" + v1 + ":]";
                            DataStack.Push(v1);
                            break;
                        case OpCode.SLICE2:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + "[:" + v1 + "]";
                            DataStack.Push(v1);
                            break;
                        case OpCode.SLICE3:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v3 = DataStack.Pop();
                            v1 = v3 + "[" + v1 + ":" + v2 + "]";
                            DataStack.Push(v1);
                            break;
                        case OpCode.STORE_SLICE:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            result.Add(v1 + "[:] = " + v2);
                            break;
                        case OpCode.STORE_SLICE1:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v3 = DataStack.Pop();
                            result.Add(v2 + "[" + v1 + ":] = " + v3);
                            break;
                        case OpCode.STORE_SLICE2:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v3 = DataStack.Pop();
                            result.Add(v2 + "[:" + v1 + "] = " + v3);
                            break;
                        case OpCode.STORE_SLICE3:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v3 = DataStack.Pop();
                            v4 = DataStack.Pop();
                            result.Add(v3 + "[" + v1 + ":" + v2 + "] = " + v4);
                            break;
                        case OpCode.DELETE_SLICE:
                            v1 = DataStack.Pop();
                            result.Add("del " + v1 + "[:]");
                            break;
                        case OpCode.DELETE_SLICE1:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            result.Add("del " + v2 + "[" + v1 + ":]");
                            break;
                        case OpCode.DELETE_SLICE2:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            result.Add("del " + v2 + "[:" + v1 + "]");
                            break;
                        case OpCode.DELETE_SLICE3:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v3 = DataStack.Pop();
                            result.Add("del " + v3 + "[" + v1 + ":" + v2 + "]");
                            break;
                        case OpCode.STORE_MAP:
                            // TODO: Validate
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v3 = DataStack.Pop();
                            result.Add(v3 + "[" + v1 + "] =" + v2);
                            break;
                        case OpCode.INPLACE_ADD:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " += " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.INPLACE_SUBTRACT:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " -= " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.INPLACE_MULTIPLY:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " *= " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.INPLACE_DIVIDE:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " /= " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.INPLACE_MODULO:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " %= " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.STORE_SUBSCR:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v3 = DataStack.Pop();
                            result.Add(v2 + "[" + v1 + "] = " + v3);
                            break;
                        case OpCode.DELETE_SUBSCR:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            result.Add("del " + v2 + "[" + v1 + "]");
                            break;
                        case OpCode.BINARY_LSHIFT:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " << " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.BINARY_RSHIFT:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " >> " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.BINARY_AND:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " & " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.BINARY_XOR:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " ^ " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.BINARY_OR:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " | " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.INPLACE_POWER:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " **= " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.GET_ITER:
                            DataStack.Push(String.Format("getiter({0})", DataStack.Pop()));
                            break;
                        case OpCode.PRINT_EXPR:
                            result.Add("print " + DataStack.Pop());
                            break;
                        case OpCode.PRINT_ITEM:
                            result.Add("print " + DataStack.Pop());
                            // TODO: Lookahead
                            break;
                        case OpCode.PRINT_NEWLINE:
                            result.Add("print");
                            break;
                        case OpCode.PRINT_ITEM_TO:
                            result.Add("print >>" + DataStack.Pop() + ", " + DataStack.Pop());
                            // TODO: Lookahead
                            break;
                        case OpCode.PRINT_NEWLINE_TO:
                            result.Add("print >>" + DataStack.Pop());
                            break;
                        case OpCode.INPLACE_LSHIFT:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " <<= " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.INPLACE_RSHIFT:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " >>= " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.INPLACE_AND:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " &= " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.INPLACE_XOR:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " ^= " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.INPLACE_OR:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " |= " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.BREAK_LOOP:
                            result.Add("break");
                            break;
                        //case OpCode.WITH_CLEANUP:
                        //    break;
                        case OpCode.LOAD_LOCALS:
                            DataStack.Push("load_locals_placeholder");
                            break;
                        case OpCode.RETURN_VALUE:
                            v1 = DataStack.Pop();
                            if (v1 != "None")
                                result.Add("return " + v1);
                            break;
                        case OpCode.IMPORT_STAR:
                            result.Add("import * from " + decoder.Name);
                            break;
                        case OpCode.EXEC_STMT:
                            result.Add("exec " + DataStack.Pop());
                            break;
                        case OpCode.YIELD_VALUE:
                            v1 = DataStack.Pop();
                            result.Add("yield " + v1);
                            break;
                        //case OpCode.POP_BLOCK:
                        //    break;
                        case OpCode.END_FINALLY:
                            v1 = DataStack.Pop();
                            result.Add("end_finally_placeholder " + v1);
                            break;
                        case OpCode.BUILD_CLASS:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            string classDefinition = String.Format("class {0} {1}:\n", v2.Trim('\''), v1);
                            string classBody = "";
                            PythonCodeObject child_co = obj.Methods.Values.FirstOrDefault();
                            if (child_co != null)
                            {
                                foreach (KeyValuePair<uint, PythonCodeObject> keyPair in child_co.Methods.OrderBy(kp => kp.Key))
                                {
                                    classBody += keyPair.Value.SourceCode + "\n";
                                }
                            }
                            result.Add(classDefinition + classBody);
                            opCode.GetNextInstruction();
                            break;
                        case OpCode.STORE_NAME:
                            v1 = DataStack.Pop();
                            if (decoder.Name == "__doc__")
                            {
                                result.Add("\"\"\"" + v1.Trim('\'') + "\"\"\"");
                            }
                            else
                            {
                                result.Add(decoder.Name + " = " + v1);
                            }
                            break;
                        case OpCode.DELETE_NAME:
                            result.Add("del " + decoder.Name);
                            break;
                        case OpCode.UNPACK_SEQUENCE:
                            int count = (int)decoder.OpArg;
                            List<string> unpackedValues = new List<string>();
                            while (count-- > 0)
                            {
                                OpCodeDecoder tempDecoder = opCode.PeekNextInstruction();
                                if (tempDecoder.OpCode == OpCode.STORE_FAST || tempDecoder.OpCode == OpCode.STORE_NAME || tempDecoder.OpCode == OpCode.STORE_GLOBAL)
                                {
                                    tempDecoder = opCode.GetNextInstruction();
                                    unpackedValues.Add(tempDecoder.OpCode == OpCode.STORE_FAST ? tempDecoder.LocalName : tempDecoder.Name);
                                }
                            }
                            v1 = DataStack.Pop();
                            result.Add(unpackedValues.Aggregate("", (res, key) => res += (res == "") ? key : ", " + key) + " = " + v1);
                            break;
                        case OpCode.FOR_ITER:
                            result.Add("for_iter" + DataStack.Pop());
                            break;
                        //case OpCode.LIST_APPEND:
                        //    break;
                        case OpCode.STORE_ATTR:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            result.Add(v1 + "." + decoder.Name + " = " + v2);
                            break;
                        case OpCode.DELETE_ATTR:
                            v1 = DataStack.Pop();
                            result.Add("del " + v1 + "." + decoder.Name);
                            break;
                        case OpCode.STORE_GLOBAL:
                            v1 = DataStack.Pop();
                            result.Add(decoder.Name + "=" + v1);
                            break;
                        case OpCode.DELETE_GLOBAL:
                            result.Add("del " + decoder.Name);
                            break;
                        case OpCode.DUP_TOPX:
                            if (decoder.OpArg == 2)
                            {
                                v1 = DataStack.ElementAt(1);
                                v2 = DataStack.ElementAt(0);
                                DataStack.Push(v1);
                                DataStack.Push(v2);
                            }
                            else if (decoder.OpArg == 3)
                            {
                                v1 = DataStack.ElementAt(2);
                                v2 = DataStack.ElementAt(1);
                                v3 = DataStack.ElementAt(0);
                                DataStack.Push(v1);
                                DataStack.Push(v2);
                                DataStack.Push(v3);
                            }
                            break;
                        case OpCode.LOAD_CONST:
                            DataStack.Push(decoder.Constant);
                            break;
                        case OpCode.LOAD_NAME:
                            DataStack.Push(decoder.Name);
                            break;
                        case OpCode.BUILD_TUPLE:
                            v1 = "(";
                            for (int i = (int)decoder.OpArg - 1; i >= 0 ; i--)
                            {
                                v2 = DataStack.ElementAt(i);
                                v1 += v2 + (i > 0 ? ", " : "");
                            }
                            v1 += ")";
                            for (int i = 0; i < (int)decoder.OpArg; i++)
                            {
                                DataStack.Pop();
                            }
                            DataStack.Push(v1);
                            break;
                        case OpCode.BUILD_LIST:
                            v1 = "[";
                            for (int i = (int)decoder.OpArg - 1; i >= 0 ; i--)
                            {
                                v2 = DataStack.ElementAt(i);
                                v1 += v2 + (i > 0 ? ", " : "");
                            }
                            for (int i = 0; i < (int)decoder.OpArg; i++)
                            {
                                DataStack.Pop();
                            }
                            v1 += "]";
                            DataStack.Push(v1);
                            break;
                        case OpCode.BUILD_SET:
                            v1 = "{";
                            for (int i = 0; i < decoder.OpArg; i++)
                            {
                                v2 = DataStack.Pop();
                                v1 += (v1 == "{") ? v2 : ", " + v2;
                            }
                            v1 += "}";
                            DataStack.Push(v1);
                            break;
                        case OpCode.BUILD_MAP:
                            if (decoder.OpArg == 0)
                            {
                                DataStack.Push("{}");
                            }
                            else
                            {
                                DataStack.Push("build_map_placeholder");
                            }
                            break;
                        case OpCode.LOAD_ATTR:
                            v1 = DataStack.Pop();
                            v1 = v1 + "." + decoder.Name;
                            DataStack.Push(v1);
                            break;
                        case OpCode.COMPARE_OP:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            DataStack.Push(String.Format("{0} {1} {2}", v2, decoder.CompareOperator, v1));
                            break;
                        case OpCode.IMPORT_NAME:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();

                            v1 = "import " + decoder.Name;
                            nextDecoder = opCode.PeekNextInstruction();
                            if (nextDecoder.OpCode == OpCode.STORE_NAME)
                            {
                                if (decoder.Name != nextDecoder.Name)
                                {
                                    v1 += " as " + nextDecoder.Name;
                                }
                                opCode.GetNextInstruction();
                                result.Add(v1);
                            }
                            else
                            {
                                v1 = "from " + decoder.Name;
                                DataStack.Push(v1);
                            }
                            break;
                        case OpCode.IMPORT_FROM:
                            v1 = DataStack.Pop();
                            v1 += (v1.Contains(" import ") ? ", " : " import ") + decoder.Name;
                            nextDecoder = opCode.PeekNextInstruction();
                            if (nextDecoder.OpCode == OpCode.STORE_NAME)
                            {
                                if (decoder.Name != nextDecoder.Name)
                                {
                                    v1 += " as " + nextDecoder.Name;
                                }
                                nextDecoder = opCode.PeekNextInstruction(2);
                                if (nextDecoder.OpCode != OpCode.IMPORT_FROM)
                                {
                                    result.Add(v1);
                                }
                                else
                                {
                                    DataStack.Push(v1);
                                }
                            }
                            else
                            {
                                DataStack.Push(v1);
                            }
                            break;
                        case OpCode.JUMP_FORWARD:
                            if (ElseStack.Count > 0 && ElseStack.Peek() == decoder.Offset && decoder.OpArg > 0)
                            {
                                ElseStack.Pop();
                                result.Add("else:");
                            }
                            break;
                        case OpCode.JUMP_IF_FALSE_OR_POP:
                            DataStack.Push(DataStack.Pop() + " or ");
                            break;
                        case OpCode.JUMP_IF_TRUE_OR_POP:
                            DataStack.Push(DataStack.Pop() + " and ");
                            break;
                        case OpCode.JUMP_ABSOLUTE:
                            if (ElseStack.Count > 0 && ElseStack.Peek() == decoder.Offset && decoder.OpArg > 0)
                            {
                                ElseStack.Pop();
                                result.Add("else:");
                            }
                            break;
                        case OpCode.POP_JUMP_IF_FALSE:
                            int jumpOffset = (int)decoder.OpArg;
                            int opIndex = opCode.GetIndexByOffset(jumpOffset);
                            nextDecoder = opCode.PeekInstructionAt(opIndex - 1);
                            if (opIndex > 0 && (nextDecoder.OpCode == OpCode.JUMP_FORWARD || nextDecoder.OpCode == OpCode.JUMP_ABSOLUTE))
                            {
                                if (ElseStack.Count == 0)
                                {
                                    ElseStack.Push(nextDecoder.Offset);
                                }else if (ElseStack.Peek() != nextDecoder.Offset)
                                {
                                    ElseStack.Push(nextDecoder.Offset);
                                }
                            }
                            result.Add("if " + DataStack.Pop() + ":");
                            break;
                        case OpCode.POP_JUMP_IF_TRUE:
                            jumpOffset = (int)decoder.OpArg;
                            opIndex = opCode.GetIndexByOffset(jumpOffset);
                            nextDecoder = opCode.PeekInstructionAt(opIndex - 1);
                            if (opIndex > 0 && (nextDecoder.OpCode == OpCode.JUMP_FORWARD || nextDecoder.OpCode == OpCode.JUMP_ABSOLUTE))
                            {
                                if (ElseStack.Count == 0)
                                {
                                    ElseStack.Push(nextDecoder.Offset);
                                }
                                else if (ElseStack.Peek() != nextDecoder.Offset)
                                {
                                    ElseStack.Push(nextDecoder.Offset);
                                }
                            }
                            result.Add("if not " + DataStack.Pop() + ":");
                            break;
                        case OpCode.LOAD_GLOBAL:
                            DataStack.Push(decoder.Name);
                            break;
                        case OpCode.CONTINUE_LOOP:
                            result.Add("continue");
                            break;
                        //case OpCode.SETUP_LOOP:
                        //    break;
                        //case OpCode.SETUP_EXCEPT:
                        //    break;
                        //case OpCode.SETUP_FINALLY:
                        //    break;
                        case OpCode.LOAD_FAST:
                            DataStack.Push(decoder.LocalName);
                            break;
                        case OpCode.STORE_FAST:
                            v1 = DataStack.Pop();
                            result.Add(decoder.LocalName + " = " + v1);
                            break;
                        case OpCode.DELETE_FAST:
                            result.Add("del " + decoder.LocalName);
                            break;
                        case OpCode.RAISE_VARARGS:
                            if (decoder.OpArg == 3)
                                v1 = "," + DataStack.Pop();
                            if (decoder.OpArg == 2)
                                v1 = "," + DataStack.Pop() + v1;
                            if (decoder.OpArg == 1)
                                v1 = DataStack.Pop() + v1;

                            result.Add("raise " + v1);
                            break;
                        case OpCode.CALL_FUNCTION:
                            uint na = decoder.OpArg & 0xFF;
                            uint nk = (decoder.OpArg >> 8) & 0xFF;
                            uint n = na + nk * 2;
                            if (n > DataStack.Count)
                                throw new ArgumentException(String.Format(@"ERROR: CALL_FUNCTION expects {0} params that bigger than stack size {1}", decoder.OpArg, DataStack.Count));
                            string pars = "";
                            for (int i = 0; i < nk; i++)
                            {
                                v1 = DataStack.Pop();
                                v2 = DataStack.Pop();
                                pars = v2 + ": " + v1 + (pars != "" ? "," : "") + pars;
                            }
                            for (int i = 0; i < na; i++)
                            {
                                pars = DataStack.Pop() + (pars != "" ? "," : "") + pars;
                            }
                            v1 = DataStack.Pop();
                            DataStack.Push(v1 + "(" + pars + ")");
                            break;
                        case OpCode.MAKE_FUNCTION:
                            string const_idx = DataStack.Pop();
                            // UGLY HACK!!!
                            child_co  = obj.Consts[int.Parse(const_idx)] as PythonCodeObject;
                            List<string> default_params = new List<string>();
                            if (decoder.OpArg > 0)
                            {
                                count = (int)decoder.OpArg;
                                while (count-- > 0)
                                    default_params.Add(DataStack.Pop());
                            }
                            child_co.SourceCode = Decompile(child_co, default_params);
                            obj.Methods[child_co.FirstLineNo] = child_co;
                            nextDecoder = opCode.PeekNextInstruction();
                            if (nextDecoder.OpCodeName.StartsWith("STORE_") || (nextDecoder.OpCodeName == "CALL_FUNCTION" && nextDecoder.OpArg == 0))
                            {
                                opCode.GetNextInstruction();
                            }
                            break;
                        case OpCode.BUILD_SLICE:
                            if (decoder.OpArg == 3)
                                v3 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = DataStack.Pop();

                            if (v3 == "None")
                                v3 = "";
                            if (v2 == "None")
                                v2 = "";
                            if (v1 == "None")
                                v1 = "";

                            if (decoder.OpArg == 3)
                            {
                                DataStack.Push(String.Format("{0}:{1}:{2}",v1,v2,v3));
                            }
                            else
                            {
                                DataStack.Push(String.Format("{0}:{1}", v1, v2));
                            }
                            break;
                        //case OpCode.MAKE_CLOSURE:
                        //    break;
                        //case OpCode.LOAD_CLOSURE:
                        //    break;
                        //case OpCode.LOAD_DEREF:
                        //    break;
                        //case OpCode.STORE_DEREF:
                        //    break;
                        case OpCode.CALL_FUNCTION_VAR:
                            na = decoder.OpArg & 0xFF;
                            nk = (decoder.OpArg >> 8) & 0xFF;
                            n = na + nk * 2 + 1;
                            if (n > DataStack.Count)
                                throw new ArgumentException(String.Format(@"ERROR: CALL_FUNCTION expects {0} params that bigger than stack size {1}", decoder.OpArg, DataStack.Count));
                            pars = "";
                            for (int i = 0; i < nk; i++)
                            {
                                v1 = DataStack.Pop();
                                v2 = DataStack.Pop();
                                pars = v2 + ": " + v1 + (pars != "" ? "," : "") + pars;
                            }
                            for (int i = 0; i < na; i++)
                            {
                                pars = DataStack.Pop() + (pars != "" ? "," : "") + pars;
                            }
                            v2 = DataStack.Pop();
                            v1 = DataStack.Pop();
                            DataStack.Push(v1 + "(" + pars + (pars != "" ? ", " : "") + "*" + v2 + ")");
                            break;
                        case OpCode.CALL_FUNCTION_KW:
                            na = decoder.OpArg & 0xFF;
                            nk = (decoder.OpArg >> 8) & 0xFF;
                            n = na + nk * 2 + 1;
                            if (n > DataStack.Count)
                                throw new ArgumentException(String.Format(@"ERROR: CALL_FUNCTION expects {0} params that bigger than stack size {1}", decoder.OpArg, DataStack.Count));
                            pars = "";
                            for (int i = 0; i < nk; i++)
                            {
                                v1 = DataStack.Pop();
                                v2 = DataStack.Pop();
                                pars = v2 + ": " + v1 + (pars != "" ? ", " : "") + pars;
                            }
                            for (int i = 0; i < na; i++)
                            {
                                pars = DataStack.Pop() + (pars != "" ? ", " : "") + pars;
                            }
                            v2 = DataStack.Pop();
                            v1 = DataStack.Pop();
                            DataStack.Push(v1 + "(" + pars + (pars != "" ? ", " : "") + "**" + v2 + ")");
                            break;
                        case OpCode.CALL_FUNCTION_VAR_KW:
                            na = decoder.OpArg & 0xFF;
                            nk = (decoder.OpArg >> 8) & 0xFF;
                            n = na + nk * 2 + 2;
                            if (n > DataStack.Count)
                                throw new ArgumentException(String.Format(@"ERROR: CALL_FUNCTION expects {0} params that bigger than stack size {1}", decoder.OpArg, DataStack.Count));
                            pars = "";
                            for (int i = 0; i < nk; i++)
                            {
                                v1 = DataStack.Pop();
                                v2 = DataStack.Pop();
                                pars = v2 + ": " + v1 + (pars != "" ? "," : "") + pars;
                            }
                            for (int i = 0; i < na; i++)
                            {
                                pars = DataStack.Pop() + (pars != "" ? "," : "") + pars;
                            }
                            v3 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = DataStack.Pop();
                            DataStack.Push(v1 + "(" + pars + (pars != "" ? ", " : "") + " *" + v2 + ", **" + v3 + ")");
                            break;
                        //case OpCode.SETUP_WITH:
                        //    break;
                        //case OpCode.EXTENDED_ARG:
                        //    break;
                        //case OpCode.SET_ADD:
                        //    break;
                        //case OpCode.MAP_ADD:
                        //    break;
                        default:
                            result.Add(decoder.OpCodeName + " " + decoder.OpArg);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    result.Add("EXCEPTION: " + ex.Message + "\n\n");
                }
            }

            return result.Aggregate("", (acc, s) => acc == "" ? s : acc + "\n" + s );
        }


        public static string Expr(PythonCodeObject obj, OpCode opCode, int endOffset = 0)
        {
            if (obj == null)
                return "";
            List<string> result = new List<string>();
            OpCodeDecoder decoder = null;
            OpCodeDecoder nextDecoder = null;
            Stack<string> DataStack = new Stack<string>();

            while (opCode.HasInstructionsToProcess)
            {
                try
                {
                    if (endOffset > 0)
                    {
                        decoder = opCode.PeekNextInstruction();
                        if (decoder.Offset > endOffset)
                            return "";
                    }
                    decoder = opCode.GetNextInstruction();
                    string v1 = "", v2 = "", v3 = "", v4 = "";

                    switch (decoder.OpCode)
                    {
                        case OpCode.POP_TOP:
                            return DataStack.Pop();
                        case OpCode.ROT_TWO:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            DataStack.Push(v1);
                            DataStack.Push(v2);
                            break;
                        case OpCode.ROT_THREE:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v3 = DataStack.Pop();
                            DataStack.Push(v1);
                            DataStack.Push(v2);
                            DataStack.Push(v3);
                            break;
                        case OpCode.DUP_TOP:
                            v1 = DataStack.Peek();
                            DataStack.Push(v1);
                            break;
                        case OpCode.ROT_FOUR:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v3 = DataStack.Pop();
                            v4 = DataStack.Pop();
                            DataStack.Push(v1);
                            DataStack.Push(v2);
                            DataStack.Push(v3);
                            DataStack.Push(v4);
                            break;
                        case OpCode.NOP:
                            break;
                        case OpCode.UNARY_POSITIVE:
                            v1 = DataStack.Pop();
                            v1 = v1[0] == '-' ? v1.Substring(1) : v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.UNARY_NEGATIVE:
                            v1 = DataStack.Pop();
                            v1 = v1[0] == '-' ? v1 : v1.Substring(1);
                            DataStack.Push(v1);
                            break;
                        case OpCode.UNARY_NOT:
                            v1 = DataStack.Pop();
                            v1 = "not " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.UNARY_CONVERT:
                            break;
                        case OpCode.UNARY_INVERT:
                            v1 = DataStack.Pop();
                            v1 = "not " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.BINARY_POWER:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " ** " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.BINARY_MULTIPLY:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " * " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.BINARY_DIVIDE:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " / " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.BINARY_MODULO:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " % " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.BINARY_ADD:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " + " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.BINARY_SUBTRACT:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " - " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.BINARY_SUBSCR:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + "[" + v1 + "]";
                            DataStack.Push(v1);
                            break;
                        case OpCode.BINARY_FLOOR_DIVIDE:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " // " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.BINARY_TRUE_DIVIDE:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " / " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.INPLACE_FLOOR_DIVIDE:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + @" //= " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.INPLACE_TRUE_DIVIDE:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " /= " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.SLICE:
                            v1 = DataStack.Pop();
                            v1 = v1 + "[:]";
                            DataStack.Push(v1);
                            break;
                        case OpCode.SLICE1:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + "[" + v1 + ":]";
                            DataStack.Push(v1);
                            break;
                        case OpCode.SLICE2:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + "[:" + v1 + "]";
                            DataStack.Push(v1);
                            break;
                        case OpCode.SLICE3:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v3 = DataStack.Pop();
                            v1 = v3 + "[" + v1 + ":" + v2 + "]";
                            DataStack.Push(v1);
                            break;
                        case OpCode.STORE_SLICE:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            return v1 + "[:] = " + v2;
                        case OpCode.STORE_SLICE1:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v3 = DataStack.Pop();
                            return v2 + "[" + v1 + ":] = " + v3;
                        case OpCode.STORE_SLICE2:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v3 = DataStack.Pop();
                            return v2 + "[:" + v1 + "] = " + v3;
                        case OpCode.STORE_SLICE3:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v3 = DataStack.Pop();
                            v4 = DataStack.Pop();
                            return v3 + "[" + v1 + ":" + v2 + "] = " + v4;
                        case OpCode.DELETE_SLICE:
                            v1 = DataStack.Pop();
                            return "del " + v1 + "[:]";
                        case OpCode.DELETE_SLICE1:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            return "del " + v2 + "[" + v1 + ":]";
                        case OpCode.DELETE_SLICE2:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            return "del " + v2 + "[:" + v1 + "]";
                        case OpCode.DELETE_SLICE3:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v3 = DataStack.Pop();
                            return "del " + v3 + "[" + v1 + ":" + v2 + "]";
                        case OpCode.STORE_MAP:
                            // TODO: Validate
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            return v1 + ": " + v2;
                        case OpCode.INPLACE_ADD:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " += " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.INPLACE_SUBTRACT:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " -= " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.INPLACE_MULTIPLY:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " *= " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.INPLACE_DIVIDE:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " /= " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.INPLACE_MODULO:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " %= " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.STORE_SUBSCR:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v3 = DataStack.Pop();
                            return v2 + "[" + v1 + "] = " + v3;
                        case OpCode.DELETE_SUBSCR:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            return "del " + v2 + "[" + v1 + "]";
                        case OpCode.BINARY_LSHIFT:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " << " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.BINARY_RSHIFT:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " >> " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.BINARY_AND:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " & " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.BINARY_XOR:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " ^ " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.BINARY_OR:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " | " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.INPLACE_POWER:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " **= " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.GET_ITER:
                            return "";
                        case OpCode.PRINT_EXPR:
                            return "print " + DataStack.Pop();
                        case OpCode.PRINT_ITEM:
                            return "print " + DataStack.Pop();
                        // TODO: Lookahead
                        case OpCode.PRINT_NEWLINE:
                            return "print";
                        case OpCode.PRINT_ITEM_TO:
                            return "print >>" + DataStack.Pop() + ", " + DataStack.Pop();
                        // TODO: Lookahead
                        case OpCode.PRINT_NEWLINE_TO:
                            return "print >>" + DataStack.Pop();
                        case OpCode.INPLACE_LSHIFT:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " <<= " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.INPLACE_RSHIFT:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " >>= " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.INPLACE_AND:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " &= " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.INPLACE_XOR:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " ^= " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.INPLACE_OR:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + " |= " + v1;
                            DataStack.Push(v1);
                            break;
                        case OpCode.BREAK_LOOP:
                            return "";
                        case OpCode.WITH_CLEANUP:
                            return "";
                        case OpCode.LOAD_LOCALS:
                            return "";
                        case OpCode.RETURN_VALUE:
                            v1 = DataStack.Pop();
                            if (v1 != "None")
                                return "return " + v1;
                            return "";
                        case OpCode.IMPORT_STAR:
                            return "import * from " + decoder.Name;
                        case OpCode.EXEC_STMT:
                            return "exec " + DataStack.Pop();
                        case OpCode.YIELD_VALUE:
                            v1 = DataStack.Pop();
                            return "yield " + v1;
                        case OpCode.POP_BLOCK:
                            return "";
                        case OpCode.END_FINALLY:
                            return "";
                        case OpCode.BUILD_CLASS:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            string classDefinition = String.Format("class {0} {1}:\n", v2.Trim('\''), v1);
                            string classBody = "";
                            PythonCodeObject child_co = obj.Methods.Values.FirstOrDefault();
                            if (child_co != null)
                            {
                                foreach (KeyValuePair<uint, PythonCodeObject> keyPair in child_co.Methods.OrderBy(kp => kp.Key))
                                {
                                    classBody += keyPair.Value.SourceCode + "\n";
                                }
                            }
                            opCode.GetNextInstruction();
                            return classDefinition + classBody;
                        case OpCode.STORE_NAME:
                            v1 = DataStack.Pop();
                            if (decoder.Name == "__doc__")
                            {
                                return "\"\"\"" + v1.Trim('\'') + "\"\"\"";
                            }
                            else
                            {
                                return decoder.Name + " = " + v1;
                            }
                        case OpCode.DELETE_NAME:
                            return "del " + decoder.Name;
                        case OpCode.UNPACK_SEQUENCE:
                            return "";
                        case OpCode.FOR_ITER:
                            return "";
                        case OpCode.LIST_APPEND:
                            return "";
                        case OpCode.STORE_ATTR:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            return v1 + "." + decoder.Name + " = " + v2;
                        case OpCode.DELETE_ATTR:
                            v1 = DataStack.Pop();
                            return "del " + v1 + "." + decoder.Name;
                        case OpCode.STORE_GLOBAL:
                            v1 = DataStack.Pop();
                            return decoder.Name + "=" + v1;
                        case OpCode.DELETE_GLOBAL:
                            return "del " + decoder.Name;
                        case OpCode.DUP_TOPX:
                            if (decoder.OpArg == 2)
                            {
                                v1 = DataStack.ElementAt(1);
                                v2 = DataStack.ElementAt(0);
                                DataStack.Push(v1);
                                DataStack.Push(v2);
                            }
                            else if (decoder.OpArg == 3)
                            {
                                v1 = DataStack.ElementAt(2);
                                v2 = DataStack.ElementAt(1);
                                v3 = DataStack.ElementAt(0);
                                DataStack.Push(v1);
                                DataStack.Push(v2);
                                DataStack.Push(v3);
                            }
                            break;
                        case OpCode.LOAD_CONST:
                            DataStack.Push(decoder.Constant);
                            break;
                        case OpCode.LOAD_NAME:
                            DataStack.Push(decoder.Name);
                            break;
                        case OpCode.BUILD_TUPLE:
                            v1 = "(";
                            for (int i = (int)decoder.OpArg - 1; i >= 0; i--)
                            {
                                v2 = DataStack.ElementAt(i);
                                v1 += v2 + (i > 0 ? ", " : "");
                            }
                            v1 += ")";
                            for (int i = 0; i < (int)decoder.OpArg; i++)
                            {
                                DataStack.Pop();
                            }
                            DataStack.Push(v1);
                            break;
                        case OpCode.BUILD_LIST:
                            v1 = "[";
                            for (int i = (int)decoder.OpArg - 1; i >= 0; i--)
                            {
                                v2 = DataStack.ElementAt(i);
                                v1 += v2 + (i > 0 ? ", " : "");
                            }
                            for (int i = 0; i < (int)decoder.OpArg; i++)
                            {
                                DataStack.Pop();
                            }
                            v1 += "]";
                            DataStack.Push(v1);
                            break;
                        case OpCode.BUILD_SET:
                            v1 = "{";
                            for (int i = 0; i < decoder.OpArg; i++)
                            {
                                v2 = DataStack.Pop();
                                v1 += (v1 == "{") ? v2 : ", " + v2;
                            }
                            v1 += "}";
                            DataStack.Push(v1);
                            break;
                        case OpCode.BUILD_MAP:
                            return "";
                        case OpCode.LOAD_ATTR:
                            v1 = DataStack.Pop();
                            v1 = v1 + "." + decoder.Name;
                            DataStack.Push(v1);
                            break;
                        case OpCode.COMPARE_OP:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            DataStack.Push(String.Format("{0} {1} {2}", v2, decoder.CompareOperator, v1));
                            break;
                        case OpCode.IMPORT_NAME:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();

                            v1 = "import " + decoder.Name;
                            nextDecoder = opCode.PeekNextInstruction();
                            if (nextDecoder.OpCode == OpCode.STORE_NAME)
                            {
                                if (decoder.Name != nextDecoder.Name)
                                {
                                    v1 += " as " + nextDecoder.Name;
                                }
                                opCode.GetNextInstruction();
                                return v1;
                            }
                            else
                            {
                                v1 = "from " + decoder.Name;
                                DataStack.Push(v1);
                            }
                            break;
                        case OpCode.IMPORT_FROM:
                            v1 = DataStack.Pop();
                            v1 += (v1.Contains(" import ") ? ", " : " import ") + decoder.Name;
                            nextDecoder = opCode.PeekNextInstruction();
                            if (nextDecoder.OpCode == OpCode.STORE_NAME)
                            {
                                if (decoder.Name != nextDecoder.Name)
                                {
                                    v1 += " as " + nextDecoder.Name;
                                }
                                nextDecoder = opCode.PeekNextInstruction(2);
                                if (nextDecoder.OpCode != OpCode.IMPORT_FROM)
                                {
                                    return v1;
                                }
                                else
                                {
                                    DataStack.Push(v1);
                                }
                            }
                            else
                            {
                                DataStack.Push(v1);
                            }
                            break;
                        case OpCode.JUMP_FORWARD:
                            return "";
                        case OpCode.JUMP_IF_FALSE_OR_POP:
                            DataStack.Push(DataStack.Pop() + " or ");
                            break;
                        case OpCode.JUMP_IF_TRUE_OR_POP:
                            DataStack.Push(DataStack.Pop() + " and ");
                            break;
                        case OpCode.JUMP_ABSOLUTE:
                            return "";
                        case OpCode.POP_JUMP_IF_FALSE:
                            return "";
                        case OpCode.POP_JUMP_IF_TRUE:
                            return "";
                        case OpCode.LOAD_GLOBAL:
                            DataStack.Push(decoder.Name);
                            break;
                        case OpCode.CONTINUE_LOOP:
                            return "";
                        case OpCode.SETUP_LOOP:
                            return "";
                        case OpCode.SETUP_EXCEPT:
                            return "";
                        case OpCode.SETUP_FINALLY:
                            return "";
                        case OpCode.LOAD_FAST:
                            DataStack.Push(decoder.LocalName);
                            break;
                        case OpCode.STORE_FAST:
                            v1 = DataStack.Pop();
                            result.Add(decoder.LocalName + " = " + v1);
                            break;
                        case OpCode.DELETE_FAST:
                            result.Add("del " + decoder.LocalName);
                            break;
                        case OpCode.RAISE_VARARGS:
                            if (decoder.OpArg == 3)
                                v1 = "," + DataStack.Pop();
                            if (decoder.OpArg == 2)
                                v1 = "," + DataStack.Pop() + v1;
                            if (decoder.OpArg == 1)
                                v1 = DataStack.Pop() + v1;

                            return "raise " + v1;
                        case OpCode.CALL_FUNCTION:
                            uint na = decoder.OpArg & 0xFF;
                            uint nk = (decoder.OpArg >> 8) & 0xFF;
                            uint n = na + nk * 2;
                            if (n > DataStack.Count)
                                throw new ArgumentException(String.Format(@"ERROR: CALL_FUNCTION expects {0} params that bigger than stack size {1}", decoder.OpArg, DataStack.Count));
                            string pars = "";
                            for (int i = 0; i < nk; i++)
                            {
                                v1 = DataStack.Pop();
                                v2 = DataStack.Pop();
                                pars = v2 + ": " + v1 + (pars != "" ? "," : "") + pars;
                            }
                            for (int i = 0; i < na; i++)
                            {
                                pars = DataStack.Pop() + (pars != "" ? "," : "") + pars;
                            }
                            v1 = DataStack.Pop();
                            DataStack.Push(v1 + "(" + pars + ")");
                            break;
                        case OpCode.MAKE_FUNCTION:
                            return "";
                        case OpCode.BUILD_SLICE:
                            if (decoder.OpArg == 3)
                                v3 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = DataStack.Pop();

                            if (v3 == "None")
                                v3 = "";
                            if (v2 == "None")
                                v2 = "";
                            if (v1 == "None")
                                v1 = "";

                            if (decoder.OpArg == 3)
                            {
                                DataStack.Push(String.Format("{0}:{1}:{2}", v1, v2, v3));
                            }
                            else
                            {
                                DataStack.Push(String.Format("{0}:{1}", v1, v2));
                            }
                            break;
                        case OpCode.MAKE_CLOSURE:
                            return "";
                        case OpCode.LOAD_CLOSURE:
                            return "";
                        case OpCode.LOAD_DEREF:
                            return "";
                        case OpCode.STORE_DEREF:
                            return "";
                        case OpCode.CALL_FUNCTION_VAR:
                            na = decoder.OpArg & 0xFF;
                            nk = (decoder.OpArg >> 8) & 0xFF;
                            n = na + nk * 2 + 1;
                            if (n > DataStack.Count)
                                throw new ArgumentException(String.Format(@"ERROR: CALL_FUNCTION expects {0} params that bigger than stack size {1}", decoder.OpArg, DataStack.Count));
                            pars = "";
                            for (int i = 0; i < nk; i++)
                            {
                                v1 = DataStack.Pop();
                                v2 = DataStack.Pop();
                                pars = v2 + ": " + v1 + (pars != "" ? "," : "") + pars;
                            }
                            for (int i = 0; i < na; i++)
                            {
                                pars = DataStack.Pop() + (pars != "" ? "," : "") + pars;
                            }
                            v2 = DataStack.Pop();
                            v1 = DataStack.Pop();
                            DataStack.Push(v1 + "(" + pars + (pars != "" ? ", " : "") + "*" + v2 + ")");
                            break;
                        case OpCode.CALL_FUNCTION_KW:
                            na = decoder.OpArg & 0xFF;
                            nk = (decoder.OpArg >> 8) & 0xFF;
                            n = na + nk * 2 + 1;
                            if (n > DataStack.Count)
                                throw new ArgumentException(String.Format(@"ERROR: CALL_FUNCTION expects {0} params that bigger than stack size {1}", decoder.OpArg, DataStack.Count));
                            pars = "";
                            for (int i = 0; i < nk; i++)
                            {
                                v1 = DataStack.Pop();
                                v2 = DataStack.Pop();
                                pars = v2 + ": " + v1 + (pars != "" ? ", " : "") + pars;
                            }
                            for (int i = 0; i < na; i++)
                            {
                                pars = DataStack.Pop() + (pars != "" ? ", " : "") + pars;
                            }
                            v2 = DataStack.Pop();
                            v1 = DataStack.Pop();
                            DataStack.Push(v1 + "(" + pars + (pars != "" ? ", " : "") + "**" + v2 + ")");
                            break;
                        case OpCode.CALL_FUNCTION_VAR_KW:
                            na = decoder.OpArg & 0xFF;
                            nk = (decoder.OpArg >> 8) & 0xFF;
                            n = na + nk * 2 + 2;
                            if (n > DataStack.Count)
                                throw new ArgumentException(String.Format(@"ERROR: CALL_FUNCTION expects {0} params that bigger than stack size {1}", decoder.OpArg, DataStack.Count));
                            pars = "";
                            for (int i = 0; i < nk; i++)
                            {
                                v1 = DataStack.Pop();
                                v2 = DataStack.Pop();
                                pars = v2 + ": " + v1 + (pars != "" ? "," : "") + pars;
                            }
                            for (int i = 0; i < na; i++)
                            {
                                pars = DataStack.Pop() + (pars != "" ? "," : "") + pars;
                            }
                            v3 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = DataStack.Pop();
                            DataStack.Push(v1 + "(" + pars + (pars != "" ? ", " : "") + " *" + v2 + ", **" + v3 + ")");
                            break;
                        case OpCode.SETUP_WITH:
                            return "";
                        case OpCode.EXTENDED_ARG:
                            return "";
                        case OpCode.SET_ADD:
                            return "";
                        case OpCode.MAP_ADD:
                            return "";
                        default:
                            return "";
                    }
                }
                catch (Exception ex)
                {
                    result.Add("EXCEPTION: " + ex.Message + "\n\n");
                }
            }

            return "";
        }

        public string MoveBack(OpCode opCode)
        {
            if (opCode.CurrentInstructionIndex > 0)
                opCode.CurrentInstructionIndex--;
            return "";
        }
    }
}
