﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace PyjReader
{
    public class PycDecompiler
    {

        public static PycResult Decompile(PythonCodeObject obj, List<string> defaults = null)
        {
            if (obj == null)
                return null;
            OpCodes code = new OpCodes(obj);
            Stack<string> DataStack = new Stack<string>();

            PycResult funcBody = Stmts(obj, code, DataStack);
            if (funcBody.Result.Count > 0)
                return funcBody;

            return null;
        }


        public static PycResult Stmts(PythonCodeObject obj, OpCodes code, Stack<string> DataStack, uint endOffset = 0)
        {
            OpCode nextOpCode = null;

            if (obj == null)
                return null;
            PycResult result = new PycResult();

            while (code.HasInstructionsToProcess)
            {
                try
                {
                    if (endOffset > 0)
                    {
                        if (code.PeekNextInstruction().Offset >= endOffset)
                        {
                            if (DataStack.Count > 0 && !result.HasResult)
                            {
                                result.Add(DataStack.Pop());
                            }
                            return result;
                        }
                    }
                    
                    code.GetNextInstruction();

                    switch (code.Current.OpCodeID)
                    {
                        case OpCodes.WITH_CLEANUP:
                            break;
                        case OpCodes.LOAD_LOCALS:
                            break;
                        case OpCodes.UNPACK_SEQUENCE:
                            break;
                        case OpCodes.POP_BLOCK:
                            break;
                        case OpCodes.JUMP_FORWARD:
                            break;
                        case OpCodes.JUMP_ABSOLUTE:
                            break;
                        case OpCodes.POP_JUMP_IF_FALSE:
                            DecompileIfExpression(obj, code, DataStack, result);
                            break;
                        case OpCodes.POP_JUMP_IF_TRUE:
                            {
                                // Used in case: assert: expr POP_JUMP_IF_TRUE LOAD_GLOBAL RAISE_VARARGS(1,2)
                                nextOpCode = code.PeekNextInstruction();
                                int raiseOffset = code.GetOffsetByOpCode(OpCodes.RAISE_VARARGS);
                                if (raiseOffset > 0 && nextOpCode.OpCodeID == OpCodes.LOAD_GLOBAL)
                                {
                                    string testCondition = DataStack.Pop();
                                    OpCode raiseOp = code.PeekInstructionAtOffset((uint)raiseOffset);
                                    string msg = null;
                                    if (raiseOp.Argument == 2)
                                    {
                                        msg = Expr(obj, code, DataStack, raiseOffset);
                                    }
                                    result.Add("assert " + testCondition + (msg == "" ? "" : ", ") + msg);
                                }
                                else
                                {
                                    DecompileIfExpression(obj, code, DataStack, result);
                                }
                            }
                            break;
                        case OpCodes.SETUP_LOOP:
                            {
                                uint jumpTarget = (uint)code.Current.JumpTarget;
                                int forOffset = -1, getIterOffset = -1, jumpOffset = -1;
                                forOffset = code.GetOffsetByOpCode(OpCodes.FOR_ITER);
                                getIterOffset = code.GetOffsetByOpCode(OpCodes.GET_ITER);
                                if (forOffset > 0)
                                {
                                    nextOpCode = code.PeekInstructionAtOffset((uint)forOffset);
                                    jumpOffset = (int)nextOpCode.JumpTarget;
                                }
                                else
                                {
                                    // TODO: Figure out cases when it can be wrong
                                    jumpOffset = code.GetOffsetByOpCode(OpCodes.JUMP_ABSOLUTE);
                                }
                                if (forOffset >= 0 && forOffset < jumpOffset && jumpOffset < jumpTarget)
                                {
                                    string inExpr = Expr(obj, code, DataStack, getIterOffset);
                                    code.GetNextInstruction();
                                    code.GetNextInstruction();
                                    uint forJump = code.Current.JumpTarget;
                                    string forExpr = Expr(obj, code, DataStack);
                                    result.Add("for " + forExpr + " in " + inExpr + ":");
                                    PycResult forBody = Stmts(obj, code, DataStack, forJump);
                                    result.Add(forBody);
                                    if (forJump < jumpTarget)
                                    {
                                        code.GetNextInstruction();
                                        if (code.Current.Argument > 0)
                                        {
                                            PycResult elseBody = Stmts(obj, code, DataStack, jumpTarget);
                                            if (elseBody.HasResult)
                                            {
                                                result.Add("else:");
                                                result.Add(elseBody);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    int ifOffset = code.GetOffsetByOpCodeName("POP_JUMP_IF_");
                                    uint elseOffset = 0;
                                    string testCondition = "";
                                    OpCode ifOp = code.PeekInstructionAtOffset((uint)ifOffset);
                                    int ifOpLine = obj.LineNoTab[ifOp.Offset];
                                    int whileLine = obj.LineNoTab[code.CurrentOffset];
                                    if (ifOpLine == whileLine)
                                    {
                                        testCondition = ExtractTestCondition(obj, code, DataStack, ref jumpOffset);
                                        elseOffset = (uint)code.PeekInstructionBeforeOffset(ifOp.JumpTarget).Offset;
                                    }
                                    else
                                    {
                                        testCondition = "1";
                                    }
                                    result.Add("while " + testCondition + ":");
                                    PycResult whileBody = Stmts(obj, code, DataStack, (uint)jumpOffset);
                                    result.Add(whileBody);
                                    code.GetNextInstruction();
                                    PycResult elseBody = Stmts(obj, code, DataStack, jumpTarget);
                                    if (elseBody.HasResult)
                                    {
                                        result.Add("else:");
                                        result.Add(elseBody);
                                    }
                                }
                            }
                            break;
                        case OpCodes.SETUP_EXCEPT:
                            {
                                result.Add("try:");
                                uint jumpTarget = (uint)code.Current.JumpTarget;
                                //TODO: Modify jumpTarget - 4 code
                                PycResult bodyStmt = Stmts(obj, code, DataStack, jumpTarget - 4);
                                result.Add(bodyStmt);
                                code.GetNextInstruction(); // POP_BLOCK
                                code.GetNextInstruction(); // jump to else block;
                                uint elseJump = (uint)code.Current.JumpTarget;
                                uint endJump = 0;
                                while (true)
                                {
                                    uint excJump = 0;
                                    string exceptHandlerType = null;
                                    string varName = null;
                                    if (code.PeekNextInstruction().OpCodeID == OpCodes.DUP_TOP)
                                    {
                                        // except handler type
                                        code.SkipInstruction();
                                        exceptHandlerType = Expr(obj, code, DataStack, code.GetOffsetByOpCode(OpCodes.COMPARE_OP));
                                        code.SkipInstruction();
                                        excJump = code.GetNextInstruction().Argument;
                                    }
                                    code.SkipInstruction();
                                    nextOpCode = code.PeekNextInstruction();
                                    if (nextOpCode.OpCodeID != OpCodes.POP_TOP)
                                    {
                                        varName = Expr(obj, code, DataStack, code.GetOffsetByOpCode(OpCodes.POP_TOP));
                                    }
                                    else
                                    {
                                        code.SkipInstruction();
                                    }
                                    code.SkipInstruction();
                                    PycResult excBody = null;
                                    if (excJump > 0) {
                                        nextOpCode = code.PeekInstructionAtOffset(excJump - 3);
                                        if (nextOpCode != null)
                                        {
                                            if (nextOpCode.OpCodeID == OpCodes.JUMP_FORWARD || nextOpCode.OpCodeID == OpCodes.JUMP_ABSOLUTE)
                                            {
                                                endJump = (uint)nextOpCode.JumpTarget;
                                            }
                                        }
                                    }
                                    endJump = (endJump == 0) ? elseJump : endJump;
                                    excBody = Stmts(obj, code, DataStack, excJump == 0 ? elseJump : excJump);
                                    if (exceptHandlerType != null && varName != null)
                                    {
                                        result.Add(String.Format("except {0} as {1}:", exceptHandlerType, varName));
                                    }else if (exceptHandlerType != null && varName == null)
                                    {
                                        result.Add(String.Format("except {0}:", exceptHandlerType));
                                    } else
                                    {
                                        result.Add("except:");
                                    }

                                    if (excBody.HasResult)
                                        result.Add(excBody);
                                    else
                                        result.Add("pass");

                                    if (excJump == 0)
                                        break;
                                    if (code.PeekInstructionAtOffset(excJump).OpCodeID == OpCodes.END_FINALLY)
                                    {
                                        code.GetNextInstruction(); // END_FINALLY
                                        break;
                                    }
                                }
                                if ((uint)code.CurrentOffset + 1 < endJump)
                                {
                                    PycResult elseBody = Stmts(obj, code, DataStack, endJump);
                                    if (elseBody.HasResult)
                                    {
                                        result.Add("else:");
                                        result.Add(elseBody);
                                    }
                                }
                            }
                            break;
                        case OpCodes.SETUP_FINALLY:
                            {
                                uint finallyBodyAddress = (uint)code.Current.JumpTarget;
                                // TODO: Re-do fiallyBodyAddress - 4
                                PycResult bodyStmts = Stmts(obj, code, DataStack, finallyBodyAddress - 4);
                                code.SkipInstruction(2);
                                int endFinallyAddress = code.GetOffsetByOpCode(OpCodes.END_FINALLY);
                                PycResult finallyBodyStmts = Stmts(obj, code, DataStack, (uint)endFinallyAddress);
                                result.Add("try:");
                                result.Add(bodyStmts);
                                result.Add("finally:");
                                result.Add(finallyBodyStmts);
                            }
                            break;
                        case OpCodes.MAKE_FUNCTION:
                        case OpCodes.MAKE_CLOSURE:
                            string const_idx = DataStack.Pop();
                            // UGLY HACK!!!
                            PythonCodeObject child_co = obj.Consts[int.Parse(const_idx)] as PythonCodeObject;

                            if (code.Current.OpCodeID == OpCodes.MAKE_CLOSURE)
                                DataStack.Pop();
                            List<string> default_params = new List<string>();
                            if (code.Current.Argument > 0)
                            {
                                int count = (int)code.Current.Argument;
                                while (count-- > 0)
                                    default_params.Add(DataStack.Pop());
                                default_params.Reverse();
                            }

                            if (child_co.Name == "<lambda>")
                            {
                                MakeLambda(child_co, DataStack, default_params);
                                break;
                            }

                            child_co.SourceCode = Decompile(child_co, default_params);
                            obj.Methods[child_co.FirstLineNo] = child_co;
                            nextOpCode = code.PeekNextInstruction();
                            child_co.FuncName = child_co.Name;
                            child_co.FuncDecos = new PycResult();
                            if (child_co.Name != "<module>")
                            {
                                string pars = "";
                                if (child_co.ArgCount > 0)
                                {
                                    for (int i = 0; i < child_co.ArgCount; i++)
                                    {
                                        string param = child_co.VarNames[i];
                                        if ((i + 1) > (child_co.ArgCount - default_params.Count))
                                            param += " = " + default_params[i - ((int)child_co.ArgCount - default_params.Count)];
                                        pars += pars == "" ? param : ", " + param;
                                    }

                                }
                                child_co.FuncParams = pars;
                            }

                            if (nextOpCode.InstructionName.StartsWith("STORE_"))
                            {
                                nextOpCode = code.GetNextInstruction();
                                child_co.FuncName = nextOpCode.Name;
                            }
                            else if (nextOpCode.InstructionName == "CALL_FUNCTION" && nextOpCode.Argument == 0 && code.PeekNextInstruction(2).OpCodeID == OpCodes.BUILD_CLASS)
                            {
                                code.GetNextInstruction();
                                break;
                            }
                            else if (nextOpCode.OpCodeID == OpCodes.CALL_FUNCTION && nextOpCode.Argument == 1)
                            {
                                while (nextOpCode.OpCodeID == OpCodes.CALL_FUNCTION && nextOpCode.Argument == 1)
                                {
                                    code.SkipInstruction();
                                    string functionDecorator = "";
                                    if (DataStack.Count > 0)
                                        functionDecorator = DataStack.Pop();
                                    child_co.FuncDecos.Add("@" + functionDecorator);
                                    nextOpCode = code.PeekNextInstruction();
                                }
                            }
                            else if (code.PeekPrevInstruction().InstructionName.StartsWith("LOAD_") && DataStack.Count > 0)
                            {
                                child_co.FuncName = DataStack.Peek();
                            }

                            if (child_co.SourceCode.HasResult && !child_co.Name.StartsWith("<"))
                            {
                                if (child_co.FuncDecos.HasResult)
                                {
                                    foreach (string functionDecorator in child_co.FuncDecos.Result)
                                    {
                                        result.Add(functionDecorator);
                                    }
                                }
                                result.Add(String.Format("\n\ndef {0}({1}):",child_co.FuncName ?? child_co.Name, child_co.FuncParams));
                                result.Add(child_co.SourceCode);
                            }

                            break;

                        case OpCodes.BUILD_CLASS:
                            {
                                string baseClass = DataStack.Pop();
                                string className = DataStack.Pop();
                                string classDefinition = String.Format("\n\nclass {0}{1}:", className.Trim('\''), baseClass == "()" ? "" : " " + baseClass);
                                PycResult classBody = null;
                                PythonCodeObject funcCodeObject = obj.Methods.Values.FirstOrDefault(co => co.FuncName == className.Trim('\''));
                                if (funcCodeObject != null)
                                {
                                    classBody = funcCodeObject.SourceCode;
                                }
                                code.GetNextInstruction();
                                result.Add(classDefinition);
                                if (classBody == null || !classBody.HasResult)
                                {
                                    result.IncreaseIndent();
                                    result.Add("pass");
                                    result.DecreaseIndent();
                                }
                                else
                                    result.Add(classBody);
                                break;
                            }

                        case OpCodes.SETUP_WITH:
                            break;
                        case OpCodes.SET_ADD:
                            break;
                        case OpCodes.MAP_ADD:
                            break;
                        default:
                            if (code.CurrentInstructionIndex >= 0)
                                code.CurrentInstructionIndex--;
                            string res = Expr(obj, code, DataStack, (int)endOffset);
                            if (res != "")
                                result.Add(res);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    result.Add(String.Format("EXCEPTION for OpCode {0} ({1}) at offset {2} : {3}\n\n", code.Current.InstructionName, code.Current.Argument, code.Current.Offset, ex.Message));
                }
            }
            return result;
        }

        private static void MakeLambda(PythonCodeObject child_co, Stack<string> DataStack, List<string> default_params)
        {
            OpCodes code = new OpCodes(child_co);
            PycResult source = Stmts(child_co, code, DataStack);
            string pars = "";
            if (child_co.ArgCount > 0)
            {
                pars = " ";
                for (int i = 0; i < child_co.ArgCount; i++)
                {
                    string param = child_co.VarNames[i];
                    if ((i + 1) > (child_co.ArgCount - default_params.Count))
                        param += " = " + default_params[i - ((int)child_co.ArgCount - default_params.Count)];
                    pars += pars == " " ? param : ", " + param;
                }

            }

            if (source.Result.Count > 1)
            {
                DataStack.Push("lambda" + pars + ": (" + String.Join(",", source.Result) + ")");
            }
            else
            {
                DataStack.Push("lambda" + pars + ": " + String.Join(",", source.Result));
            }

        }

        private static void DecompileIfExpression(PythonCodeObject obj, OpCodes code, Stack<string> DataStack, PycResult result)
        {
            int jumpOffset = (int)code.Current.JumpTarget;
            int opIndex = code.GetIndexByOffset(jumpOffset);
            OpCode jumpOpcode = code.PeekInstructionAt(opIndex - 1);
            string ifTest = "";
            PycResult ifBody = null;
            PycResult ifElseBody = null;
            uint sOff, eOff;

            int line = code.GetLineOffsetRangeForOffset((uint)code.Current.Offset, out sOff, out eOff);
            bool isIfExp = code.CheckIfOpCodesExistsInLine(new List<byte> { OpCodes.POP_JUMP_IF_FALSE, OpCodes.JUMP_FORWARD }, sOff, eOff);

            if (!isIfExp)
                ifTest = DataStack.Pop();
            ifTest += ExtractTestCondition(obj, code, DataStack, ref jumpOffset);

            ifBody = Stmts(obj, code, DataStack, (uint)jumpOffset);
            if (code.Current.OpCodeID == OpCodes.JUMP_FORWARD)
            {
                if (code.Current.Offset < jumpOffset && code.Current.Argument > 0)
                {
                    uint jumpTarget = (uint)code.Current.JumpTarget;
                    ifElseBody = Stmts(obj, code, DataStack, jumpTarget);
                }
            }
            if (isIfExp)
            {
                DataStack.Push(ifBody.Result[0] + " if " + ifTest + " else " + ifElseBody.Result[0]);
            }
            else
            {
                result.Add("if " + ifTest + ":");
                result.Add(ifBody);
                if (ifElseBody != null && ifElseBody.HasResult)
                {
                    result.Add("else:");
                    result.Add(ifElseBody);
                }
            }
        }

        private static string ExtractTestCondition(PythonCodeObject obj, OpCodes code, Stack<string> DataStack, ref int jumpOffset)
        {
            uint sOff, eOff;
            int line;
            string ifTest = "";

            line = code.GetLineOffsetRangeForOffset((uint)code.Current.Offset, out sOff, out eOff);

            int jif = code.GetReversedOffsetByOpCode(OpCodes.POP_JUMP_IF_FALSE, (int)eOff, (int)sOff);
            int jit = code.GetReversedOffsetByOpCode(OpCodes.POP_JUMP_IF_TRUE, (int)eOff, (int)sOff);

            if (jif > 0 || jit > 0)
            {
                eOff = (uint)Math.Max(jif, jit);
            }

            if (code.CurrentOffset < eOff)
            {
                while (code.CurrentOffset < eOff)
                {
                    string op = "";
                    if (code.Current.OpCodeID == OpCodes.POP_JUMP_IF_FALSE)
                        op = " and ";
                    else if (code.Current.OpCodeID == OpCodes.POP_JUMP_IF_TRUE)
                        op = " or ";
                    string temp = Expr(obj, code, DataStack, (int)eOff);
                    if (temp == "")
                    {
                        temp = DataStack.Pop();
                    }
                    ifTest += op + temp;
                    code.SkipInstruction();
                    jumpOffset = (int)code.Current.JumpTarget;
                }
            }

            return ifTest;
        }

        public static string Expr(PythonCodeObject obj, OpCodes code, Stack<string> DataStack, int endOffset = 0)
        {
            if (obj == null || endOffset == -1)
                return "";
            OpCode nextOpCode = null;

            while (code.HasInstructionsToProcess)
            {
                try
                {
                    if (endOffset > 0)
                    {
                        if (code.PeekNextInstruction().Offset >= endOffset)
                        {
                            if (DataStack.Count > 0)
                                return DataStack.Pop();
                            else
                                return "";
                        }
                    }
                    code.GetNextInstruction();

                    switch (code.Current.OpCodeID)
                    {
                        case OpCodes.POP_TOP:
                            return DataStack.Pop();
                        case OpCodes.ROT_TWO:
                            {
                                string value1 = DataStack.Pop();
                                string value2 = DataStack.Pop();
                                DataStack.Push(value1);
                                DataStack.Push(value2);
                            }
                            break;
                        case OpCodes.ROT_THREE:
                            {
                                string value1 = DataStack.Pop();
                                string value2 = DataStack.Pop();
                                string value3 = DataStack.Pop();
                                DataStack.Push(value1);
                                DataStack.Push(value2);
                                DataStack.Push(value3);
                            }
                            break;
                        case OpCodes.DUP_TOP:
                            {
                                string value = DataStack.Peek();
                                DataStack.Push(value);
                            }
                            break;
                        case OpCodes.ROT_FOUR:
                            {
                                string value1 = DataStack.Pop();
                                string value2 = DataStack.Pop();
                                string value3 = DataStack.Pop();
                                string value4 = DataStack.Pop();
                                DataStack.Push(value1);
                                DataStack.Push(value2);
                                DataStack.Push(value3);
                                DataStack.Push(value4);
                            }
                            break;
                        case OpCodes.NOP:
                            break;
                        case OpCodes.UNARY_POSITIVE:
                            {
                                string value = DataStack.Pop();
                                value = value[0] == '-' ? value.Substring(1) : value;
                                DataStack.Push(value);
                            }
                            break;
                        case OpCodes.UNARY_NEGATIVE:
                            {
                                string value = DataStack.Pop();
                                value = value[0] == '-' ? value : value.Substring(1);
                                DataStack.Push(value);
                            }
                            break;
                        case OpCodes.UNARY_NOT:
                            {
                                string value = DataStack.Pop();
                                value = "not " + value;
                                DataStack.Push(value);
                            }
                            break;
                        case OpCodes.UNARY_CONVERT:
                            break;
                        case OpCodes.UNARY_INVERT:
                            {
                                string value = DataStack.Pop();
                                value = "~" + value;
                                DataStack.Push(value);
                            }
                            break;
                        case OpCodes.BINARY_POWER:
                            {
                                string rVal = DataStack.Pop();
                                string lVal = DataStack.Pop();
                                if ((rVal.Contains(" - ") || rVal.Contains(" + ")) && !rVal.Contains("("))
                                    rVal = "(" + rVal + ")";
                                if ((lVal.Contains(" - ") || lVal.Contains(" + ")) && !lVal.Contains("("))
                                    lVal = "(" + lVal + ")";
                                DataStack.Push(lVal + " ** " + rVal);
                            }
                            break;
                        case OpCodes.BINARY_MULTIPLY:
                            {
                                string rVal = DataStack.Pop();
                                string lVal = DataStack.Pop();
                                if ((rVal.Contains(" - ") || rVal.Contains(" + ")) && !rVal.Contains("("))
                                    rVal = "(" + rVal + ")";
                                if ((lVal.Contains(" - ") || lVal.Contains(" + ")) && !lVal.Contains("("))
                                    lVal = "(" + lVal + ")";
                                DataStack.Push(lVal + " * " + rVal);
                            }
                            break;
                        case OpCodes.BINARY_DIVIDE:
                            {
                                string rVal = DataStack.Pop();
                                string lVal = DataStack.Pop();
                                if ((rVal.Contains(" - ") || rVal.Contains(" + ")) && !rVal.Contains("("))
                                    rVal = "(" + rVal + ")";
                                if ((lVal.Contains(" - ") || lVal.Contains(" + ")) && !lVal.Contains("("))
                                    lVal = "(" + lVal + ")";
                                DataStack.Push(lVal + " / " + rVal);
                            }
                            break;
                        case OpCodes.BINARY_MODULO:
                            {
                                string rVal = DataStack.Pop();
                                string lVal = DataStack.Pop();
                                if ((rVal.Contains(" - ") || rVal.Contains(" + ")) && !rVal.Contains("("))
                                    rVal = "(" + rVal + ")";
                                if ((lVal.Contains(" - ") || lVal.Contains(" + ")) && !lVal.Contains("("))
                                    lVal = "(" + lVal + ")";
                                DataStack.Push(lVal + " % " + rVal);
                            }
                            break;
                        case OpCodes.BINARY_ADD:
                            {
                                string rVal = DataStack.Pop();
                                string lVal = DataStack.Pop();
                                DataStack.Push(lVal + " + " + rVal);
                            }
                            break;
                        case OpCodes.BINARY_SUBTRACT:
                            {
                                string rVal = DataStack.Pop();
                                string lVal = DataStack.Pop();
                                DataStack.Push(lVal + " - " + rVal);
                            }
                            break;
                        case OpCodes.BINARY_SUBSCR:
                            {
                                string rVal = DataStack.Pop();
                                string lVal = DataStack.Pop();
                                DataStack.Push(String.Format("{0}[{1}]",lVal,rVal));
                            }
                            break;
                        case OpCodes.BINARY_FLOOR_DIVIDE:
                            {
                                string rVal = DataStack.Pop();
                                string lVal = DataStack.Pop();
                                if ((rVal.Contains(" - ") || rVal.Contains(" + ")) && !rVal.Contains("("))
                                    rVal = "(" + rVal + ")";
                                if ((lVal.Contains(" - ") || lVal.Contains(" + ")) && !lVal.Contains("("))
                                    lVal = "(" + lVal + ")";
                                DataStack.Push(lVal + " // " + rVal);
                            }
                            break;
                        case OpCodes.BINARY_TRUE_DIVIDE:
                            {
                                string rVal = DataStack.Pop();
                                string lVal = DataStack.Pop();
                                if ((rVal.Contains(" - ") || rVal.Contains(" + ")) && !rVal.Contains("("))
                                    rVal = "(" + rVal + ")";
                                if ((lVal.Contains(" - ") || lVal.Contains(" + ")) && !lVal.Contains("("))
                                    lVal = "(" + lVal + ")";
                                DataStack.Push(lVal + " / " + rVal);
                            }
                            break;
                        case OpCodes.INPLACE_FLOOR_DIVIDE:
                            {
                                string rVal = DataStack.Pop();
                                string lVal = DataStack.Pop();
                                if ((rVal.Contains(" - ") || rVal.Contains(" + ")) && !rVal.Contains("("))
                                    rVal = "(" + rVal + ")";
                                if ((lVal.Contains(" - ") || lVal.Contains(" + ")) && !lVal.Contains("("))
                                    lVal = "(" + lVal + ")";
                                DataStack.Push(lVal + " // " + rVal);
                            }
                            break;
                        case OpCodes.INPLACE_TRUE_DIVIDE:
                            {
                                string rVal = DataStack.Pop();
                                string lVal = DataStack.Pop();
                                if ((rVal.Contains(" - ") || rVal.Contains(" + ")) && !rVal.Contains("("))
                                    rVal = "(" + rVal + ")";
                                if ((lVal.Contains(" - ") || lVal.Contains(" + ")) && !lVal.Contains("("))
                                    lVal = "(" + lVal + ")";
                                DataStack.Push(lVal + " / " + rVal);
                            }
                            break;
                        case OpCodes.SLICE:
                            DataStack.Push(DataStack.Pop() + "[:]");
                            break;
                        case OpCodes.SLICE1:
                            {
                                string startIndex = DataStack.Pop();
                                string name = DataStack.Pop();
                                DataStack.Push(name + "[" + startIndex + ":]");
                            }
                            break;
                        case OpCodes.SLICE2:
                            {
                                string endIndex = DataStack.Pop();
                                string name = DataStack.Pop();
                                DataStack.Push(name + "[:" + endIndex + "]");
                            }
                            break;
                        case OpCodes.SLICE3:
                            {
                                string startIndex = DataStack.Pop();
                                string endIndex = DataStack.Pop();
                                string name = DataStack.Pop();
                                DataStack.Push(name + "[" + startIndex + ":" + endIndex + "]");
                            }
                            break;
                        case OpCodes.STORE_SLICE:
                            {
                                string name = DataStack.Pop();
                                if (DataStack.Count == 0)
                                    return name + "[:]";
                                string value = DataStack.Pop();
                                string expr = name + "[:] = " + value;
                                if (code.PeekNextInstruction().OpCodeID == OpCodes.DUP_TOP)
                                {
                                    code.SkipInstruction();
                                    DataStack.Push(expr);
                                    break;
                                }
                                else
                                {
                                    return expr;
                                }
                            }
                        case OpCodes.STORE_SLICE1:
                            {
                                string startIndex = DataStack.Pop();
                                string name = DataStack.Pop();
                                if (DataStack.Count > 0)
                                    return name + "[" + startIndex + ":]";
                                string value = DataStack.Pop();
                                string expr = name + "[" + startIndex + ":] = " + value;
                                if (code.PeekNextInstruction().OpCodeID == OpCodes.DUP_TOP)
                                {
                                    code.SkipInstruction();
                                    DataStack.Push(expr);
                                    break;
                                }
                                else
                                {
                                    return expr;
                                }

                            }
                        case OpCodes.STORE_SLICE2:
                            {
                                string endIndex = DataStack.Pop();
                                string name = DataStack.Pop();
                                if (DataStack.Count > 0)
                                    return name + "[:" + endIndex + "]";
                                string value = DataStack.Pop();
                                string expr = name + "[" + endIndex + ":] = " + value;
                                if (code.PeekNextInstruction().OpCodeID == OpCodes.DUP_TOP)
                                {
                                    code.SkipInstruction();
                                    DataStack.Push(expr);
                                    break;
                                }
                                else
                                {
                                    return expr;
                                }
                            }
                        case OpCodes.STORE_SLICE3:
                            {
                                string startIndex = DataStack.Pop();
                                string endIndex = DataStack.Pop();
                                string name = DataStack.Pop();
                                if (DataStack.Count > 0)
                                    return name + "[" + startIndex + ":" + endIndex + "]";
                                string value = DataStack.Pop();
                                string expr = name + "[" + startIndex + ":" + endIndex + ":] = " + value;
                                if (code.PeekNextInstruction().OpCodeID == OpCodes.DUP_TOP)
                                {
                                    code.SkipInstruction();
                                    DataStack.Push(expr);
                                    break;
                                }
                                else
                                {
                                    return expr;
                                }
                            }
                        case OpCodes.DELETE_SLICE:
                            return "del " + DataStack.Pop() + "[:]";
                        case OpCodes.DELETE_SLICE1:
                            {
                                string startIndex = DataStack.Pop();
                                string name = DataStack.Pop();
                                return "del " + name + "[" + startIndex + ":]";
                            }
                        case OpCodes.DELETE_SLICE2:
                            {
                                string endIndex = DataStack.Pop();
                                string name = DataStack.Pop();
                                return "del " + name + "[:" + endIndex + "]";
                            }
                        case OpCodes.DELETE_SLICE3:
                            {
                                string startIndex = DataStack.Pop();
                                string endIndex = DataStack.Pop();
                                string name = DataStack.Pop();
                                return "del " + name + "[" + startIndex + ":" + endIndex + "]";
                            }
                        case OpCodes.STORE_MAP:
                            {
                                string key = DataStack.Pop();
                                string value = DataStack.Pop();
                                return key + ": " + value;
                            }
                        case OpCodes.INPLACE_ADD:
                            {
                                string rVal = DataStack.Pop();
                                string lVal = DataStack.Pop();
                                DataStack.Push(lVal + " + " + rVal);
                            }
                            break;
                        case OpCodes.INPLACE_SUBTRACT:
                            {
                                string rVal = DataStack.Pop();
                                string lVal = DataStack.Pop();
                                DataStack.Push(lVal + " - " + rVal);
                            }
                            break;
                        case OpCodes.INPLACE_MULTIPLY:
                            {
                                string rVal = DataStack.Pop();
                                string lVal = DataStack.Pop();
                                if ((rVal.Contains(" - ") || rVal.Contains(" + ")) && !rVal.Contains("("))
                                    rVal = "(" + rVal + ")";
                                if ((lVal.Contains(" - ") || lVal.Contains(" + ")) && !lVal.Contains("("))
                                    lVal = "(" + lVal + ")";
                                DataStack.Push(lVal + " * " + rVal);
                            }
                            break;
                        case OpCodes.INPLACE_DIVIDE:
                            {
                                string rVal = DataStack.Pop();
                                string lVal = DataStack.Pop();
                                if ((rVal.Contains(" - ") || rVal.Contains(" + ")) && !rVal.Contains("("))
                                    rVal = "(" + rVal + ")";
                                if ((lVal.Contains(" - ") || lVal.Contains(" + ")) && !lVal.Contains("("))
                                    lVal = "(" + lVal + ")";
                                DataStack.Push(lVal + " / " + rVal);
                            }
                            break;
                        case OpCodes.INPLACE_MODULO:
                            {
                                string rVal = DataStack.Pop();
                                string lVal = DataStack.Pop();
                                if ((rVal.Contains(" - ") || rVal.Contains(" + ")) && !rVal.Contains("("))
                                    rVal = "(" + rVal + ")";
                                if ((lVal.Contains(" - ") || lVal.Contains(" + ")) && !lVal.Contains("("))
                                    lVal = "(" + lVal + ")";
                                DataStack.Push(lVal + " % " + rVal);
                            }
                            break;
                        case OpCodes.STORE_SUBSCR:
                            {
                                string subscript = DataStack.Pop();
                                string name = DataStack.Pop();
                                if (DataStack.Count == 0)
                                    return name + "[" + subscript + "]";
                                string value = DataStack.Pop();
                                string expr = name + "[" + subscript + "] = " + value;
                                if (code.PeekNextInstruction().OpCodeID == OpCodes.DUP_TOP)
                                {
                                    code.SkipInstruction();
                                    DataStack.Push(expr);
                                    break;
                                }
                                else
                                {
                                    return expr;
                                }
                            }
                        case OpCodes.DELETE_SUBSCR:
                            {
                                string subscript = DataStack.Pop();
                                string name = DataStack.Pop();
                                return "del " + name + "[" + subscript + "]";
                            }
                        case OpCodes.BINARY_LSHIFT:
                            {
                                string rVal = DataStack.Pop();
                                string lVal = DataStack.Pop();
                                DataStack.Push(lVal + " << " + rVal);
                            }
                            break;
                        case OpCodes.BINARY_RSHIFT:
                            {
                                string rVal = DataStack.Pop();
                                string lVal = DataStack.Pop();
                                DataStack.Push(lVal + " >> " + rVal);
                            }
                            break;
                        case OpCodes.BINARY_AND:
                            {
                                string rVal = DataStack.Pop();
                                string lVal = DataStack.Pop();
                                DataStack.Push(lVal + " & " + rVal);
                            }
                            break;
                        case OpCodes.BINARY_XOR:
                            {
                                string rVal = DataStack.Pop();
                                string lVal = DataStack.Pop();
                                DataStack.Push(lVal + " ^ " + rVal);
                            }
                            break;
                        case OpCodes.BINARY_OR:
                            {
                                string rVal = DataStack.Pop();
                                string lVal = DataStack.Pop();
                                DataStack.Push(lVal + " | " + rVal);
                            }
                            break;
                        case OpCodes.INPLACE_POWER:
                            {
                                string rVal = DataStack.Pop();
                                string lVal = DataStack.Pop();
                                if ((rVal.Contains(" - ") || rVal.Contains(" + ")) && !rVal.Contains("("))
                                    rVal = "(" + rVal + ")";
                                if ((lVal.Contains(" - ") || lVal.Contains(" + ")) && !lVal.Contains("("))
                                    lVal = "(" + lVal + ")";
                                DataStack.Push(lVal + " ** " + rVal);
                            }
                            break;
                        case OpCodes.GET_ITER:
                            if ((DataStack.Count > 1 && 
                                code.PeekNextInstruction().OpCodeID == OpCodes.CALL_FUNCTION && code.PeekNextInstruction().Argument == 1 
                                && code.PeekNextInstruction(2).OpCodeID == OpCodes.CALL_FUNCTION && code.PeekNextInstruction(2).Argument == 1) ||
                                (DataStack.Count >= 1 &&
                                code.PeekNextInstruction().OpCodeID == OpCodes.CALL_FUNCTION && code.PeekNextInstruction().Argument == 1)
                               )
                            {
                                code.SkipInstruction(DataStack.Count > 1 ? 2 : 1);
                                string iter = DataStack.Pop();
                                string funcName = "";
                                if (DataStack.Count > 0)
                                    funcName = DataStack.Pop();
                                int makeFunctionOffset = code.GetBackOffsetByOpCodeName("MAKE_");
                                OpCode objectConst = code.PeekInstructionBeforeOffset((uint)makeFunctionOffset);
                                if (objectConst == null)
                                    throw new ArgumentNullException();
                                PythonCodeObject co = obj.Consts[(int)objectConst.Argument] as PythonCodeObject;
                                string source = co.SourceCode.Result[0].Replace(".0", iter );
                                DataStack.Push(String.Format("{0}({1})", funcName, source));
                                break;
                            }
                            return "";
                        case OpCodes.PRINT_EXPR:
                        case OpCodes.PRINT_ITEM:
                            if (code.PeekNextInstruction().OpCodeID == OpCodes.PRINT_NEWLINE)
                            {
                                code.SkipInstruction();
                                return "print " + DataStack.Pop();
                            }
                            else
                            {
                                return "print " + DataStack.Pop() + ",";
                            }
                        case OpCodes.PRINT_NEWLINE:
                            break;
                        case OpCodes.PRINT_ITEM_TO:
                            {
                                string separator = "";
                                if (code.PeekNextInstruction().OpCodeID == OpCodes.PRINT_NEWLINE_TO)
                                {
                                    separator = ",";
                                }
                                string printStmt = "print >> " + DataStack.Pop() + ", " + DataStack.Pop() + separator;
                                if (code.PeekNextInstruction().OpCodeID == OpCodes.PRINT_NEWLINE_TO)
                                {
                                    code.SkipInstruction();
                                    DataStack.Pop();
                                }
                                return printStmt;
                            }
                        case OpCodes.PRINT_NEWLINE_TO:
                            break;
                        case OpCodes.INPLACE_LSHIFT:
                            {
                                string rVal = DataStack.Pop();
                                string lVal = DataStack.Pop();
                                if ((rVal.Contains(" - ") || rVal.Contains(" + ")) && !rVal.Contains("("))
                                    rVal = "(" + rVal + ")";
                                if ((lVal.Contains(" - ") || lVal.Contains(" + ")) && !lVal.Contains("("))
                                    lVal = "(" + lVal + ")";
                                DataStack.Push(lVal + " << " + rVal);
                            }
                            break;
                        case OpCodes.INPLACE_RSHIFT:
                            {
                                string rVal = DataStack.Pop();
                                string lVal = DataStack.Pop();
                                if ((rVal.Contains(" - ") || rVal.Contains(" + ")) && !rVal.Contains("("))
                                    rVal = "(" + rVal + ")";
                                if ((lVal.Contains(" - ") || lVal.Contains(" + ")) && !lVal.Contains("("))
                                    lVal = "(" + lVal + ")";
                                DataStack.Push(lVal + " >> " + rVal);
                            }
                            break;
                        case OpCodes.INPLACE_AND:
                            {
                                string rVal = DataStack.Pop();
                                string lVal = DataStack.Pop();
                                if ((rVal.Contains(" - ") || rVal.Contains(" + ")) && !rVal.Contains("("))
                                    rVal = "(" + rVal + ")";
                                if ((lVal.Contains(" - ") || lVal.Contains(" + ")) && !lVal.Contains("("))
                                    lVal = "(" + lVal + ")";
                                DataStack.Push(lVal + " & " + rVal);
                            }
                            break;
                        case OpCodes.INPLACE_XOR:
                            {
                                string rVal = DataStack.Pop();
                                string lVal = DataStack.Pop();
                                if ((rVal.Contains(" - ") || rVal.Contains(" + ")) && !rVal.Contains("("))
                                    rVal = "(" + rVal + ")";
                                if ((lVal.Contains(" - ") || lVal.Contains(" + ")) && !lVal.Contains("("))
                                    lVal = "(" + lVal + ")";
                                DataStack.Push(lVal + " ^ " + rVal);
                            }
                            break;
                        case OpCodes.INPLACE_OR:
                            {
                                string rVal = DataStack.Pop();
                                string lVal = DataStack.Pop();
                                if ((rVal.Contains(" - ") || rVal.Contains(" + ")) && !rVal.Contains("("))
                                    rVal = "(" + rVal + ")";
                                if ((lVal.Contains(" - ") || lVal.Contains(" + ")) && !lVal.Contains("("))
                                    lVal = "(" + lVal + ")";
                                DataStack.Push(lVal + " | " + rVal);
                            }
                            break;
                        case OpCodes.BREAK_LOOP:
                            return "break";
                        case OpCodes.WITH_CLEANUP:
                            return "";
                        case OpCodes.LOAD_LOCALS:
                            return "";
                        case OpCodes.RETURN_VALUE:
                            {
                                string expr = "";
                                if (DataStack.Count > 1)
                                {
                                    expr = DataStack.Pop();
                                    while (DataStack.Count > 0)
                                    {
                                        if (DataStack.Peek().EndsWith(" and ") || DataStack.Peek().EndsWith(" or "))
                                        {
                                            expr = DataStack.Pop() + expr;
                                        }
                                        else
                                            break;
                                    }
                                }
                                else if (DataStack.Count > 0)
                                    expr = DataStack.Pop();
                                else if (code.PeekPrevInstruction().OpCodeID == OpCodes.LOAD_LOCALS)
                                    return "";

                                if (new[]{"<lambda>", "<dictcomp>", "<setcomp>"}.Contains(obj.Name))
                                {
                                    return expr;
                                }

                                if (code.Current.Offset + 1 < obj.Code.Length)
                                {
                                    return "return" + (expr == "None" ? "" : " " + expr);
                                }
                                else if (obj.Code.Length < 5)
                                {
                                    return "return" + (expr == "None" ? "" : " " + expr);
                                }
                                else
                                {
                                    if (expr != "None" && expr != "")
                                        return "return " + expr;
                                    return "";
                                }
                            }
                        case OpCodes.IMPORT_STAR:
                            break;
                        case OpCodes.EXEC_STMT:
                            return "exec " + DataStack.Pop();
                        case OpCodes.YIELD_VALUE:
                            return "yield " + DataStack.Pop();
                        case OpCodes.POP_BLOCK:
                            return "";
                        case OpCodes.END_FINALLY:
                            return "";
                        case OpCodes.BUILD_CLASS:
                            return MoveBack(code);
                        case OpCodes.STORE_NAME:
                            {
                                if (DataStack.Count == 0 || code.PeekPrevInstruction().OpCodeID == OpCodes.FOR_ITER)
                                    return code.Current.Name;

                                string value = DataStack.Pop();
                                if (code.Current.Name == "__module__" && value == "__name__")
                                {
                                    return "";
                                }
                                else if (code.Current.Name == "__doc__")
                                {
                                    return "\"\"\"" + value.Trim('\'') + "\"\"\"";
                                }
                                else
                                {
                                    string expr = code.Current.Name + " = " + value;

                                    if (code.PeekNextInstruction().OpCodeID == OpCodes.DUP_TOP)
                                    {
                                        code.SkipInstruction();
                                        DataStack.Push(expr);
                                        break;
                                    }
                                    else
                                    {
                                        return expr;
                                    }

                                }
                            }
                        case OpCodes.DELETE_NAME:
                            return "del " + code.Current.Name;
                        case OpCodes.UNPACK_SEQUENCE:
                            {
                                OpCode prevOp = code.PeekPrevInstruction();
                                int count = (int)code.Current.Argument;
                                string outExpr = "";
                                string rVal = "";
                                if (prevOp.OpCodeID != OpCodes.FOR_ITER)
                                    rVal = DataStack.Pop();
                                for (int idx = 0; idx < count; idx++)
                                {
                                    string name = Identifier(obj, code, DataStack);
                                    outExpr += (idx > 0 ? ", " : "") + name;
                                }
                                if (prevOp.OpCodeID == OpCodes.FOR_ITER)
                                    return outExpr;
                                return outExpr + " = " + rVal;
                            }
                        case OpCodes.FOR_ITER:
                            nextOpCode = code.PeekPrevInstruction();
                            if (nextOpCode.OpCodeID == OpCodes.LOAD_FAST && nextOpCode.Argument == 0)
                            {
                                if (new[] {"<genexpr>", "<dictcomp>", "<setcomp>" }.Contains(obj.Name))
                                {
                                    DataStack.Pop();
                                    string target = Expr(obj, code, DataStack);
                                    string ifExpr = Expr(obj, code, DataStack);
                                    string yieldExpr = "";
                                    if (ifExpr.StartsWith("yield "))
                                    {
                                        yieldExpr = ifExpr;
                                        ifExpr = "";
                                    }
                                    else
                                    {
                                        yieldExpr = Expr(obj, code, DataStack);
                                    }

                                    yieldExpr = yieldExpr.Replace("yield ", "");
                                    if (yieldExpr[0] == '(')
                                        yieldExpr = yieldExpr.Substring(1,yieldExpr.Length - 2);
                                    code.SkipInstruction(2);

                                    return String.Format("{0} for {1} in {2} {3}", yieldExpr, target, nextOpCode.LocalName, ifExpr);
                                }
                                else
                                {
                                    DataStack.Pop();
                                    string target = Expr(obj, code, DataStack);
                                    string ifExpr = Expr(obj, code, DataStack);
                                    code.GetNextInstruction();
                                    string yieldExpr = Expr(obj, code, DataStack).Replace("yield ", "");
                                    code.GetNextInstruction();
                                    code.GetNextInstruction();

                                    return String.Format("{0} for {1} in {2} if {3}", yieldExpr, target, nextOpCode.LocalName, ifExpr);
                                }
                            }
                            return "";
                        case OpCodes.LIST_APPEND:
                            return ""; //MoveBack(code);
                        case OpCodes.STORE_ATTR:
                            {
                                string name = DataStack.Pop();
                                if (DataStack.Count == 0 || code.PeekPrevInstruction().OpCodeID == OpCodes.FOR_ITER)
                                    return name + "." + code.Current.Name;
                                string value = DataStack.Pop();
                                string expr = name + "." + code.Current.Name + " = " + value;
                                if (code.PeekNextInstruction().OpCodeID == OpCodes.DUP_TOP)
                                {
                                    code.SkipInstruction();
                                    DataStack.Push(expr);
                                    break;
                                }
                                else
                                {
                                    return expr;
                                }
                            }
                        case OpCodes.DELETE_ATTR:
                            return "del " + DataStack.Pop() + "." + code.Current.Name;
                        case OpCodes.STORE_GLOBAL:
                            {
                                if (DataStack.Count == 0 || code.PeekPrevInstruction().OpCodeID == OpCodes.FOR_ITER)
                                    return code.Current.Name;
                                string expr = code.Current.Name + "=" + DataStack.Pop();
                                if (code.PeekNextInstruction().OpCodeID == OpCodes.DUP_TOP)
                                {
                                    code.SkipInstruction();
                                    DataStack.Push(expr);
                                    break;
                                }
                                else
                                {
                                    return expr;
                                }
                            }
                        case OpCodes.DELETE_GLOBAL:
                            return "del " + code.Current.Name;
                        case OpCodes.DUP_TOPX:
                            if (code.Current.Argument == 2)
                            {
                                string value1 = DataStack.ElementAt(1);
                                string value2 = DataStack.ElementAt(0);
                                DataStack.Push(value1);
                                DataStack.Push(value2);
                            }
                            else if (code.Current.Argument == 3)
                            {
                                string value1 = DataStack.ElementAt(2);
                                string value2 = DataStack.ElementAt(1);
                                string value3 = DataStack.ElementAt(0);
                                DataStack.Push(value1);
                                DataStack.Push(value2);
                                DataStack.Push(value3);
                            }
                            break;
                        case OpCodes.LOAD_CONST:
                            DataStack.Push(code.Current.Constant);
                            break;
                        case OpCodes.LOAD_NAME:
                            DataStack.Push(code.Current.Name);
                            break;
                        case OpCodes.BUILD_LIST:
                            {
                                int forOffset = -1, getIterOffset = -1, jumpOffset = -1, setupLoopOffset = -1;
                                forOffset = code.GetOffsetByOpCode(OpCodes.FOR_ITER);
                                setupLoopOffset = code.GetOffsetByOpCode(OpCodes.SETUP_LOOP);
                                if (code.Current.Argument == 0 && (forOffset == -1 || (setupLoopOffset > 0 && forOffset > setupLoopOffset) || (forOffset - code.Current.Offset) > 35))
                                {
                                    DataStack.Push("[]");
                                }
                                else if (code.Current.Argument == 0)
                                {
                                    string comp_for = "";
                                    forOffset = code.GetOffsetByOpCode(OpCodes.FOR_ITER);
                                    getIterOffset = code.GetOffsetByOpCode(OpCodes.GET_ITER);
                                    if (forOffset > 0)
                                    {
                                        nextOpCode = code.PeekInstructionAtOffset((uint)forOffset);
                                        jumpOffset = (int)nextOpCode.JumpTarget - 3;
                                    }
                                    else
                                    {
                                        // TODO: Figure out cases when it can be wrong
                                        jumpOffset = code.GetOffsetByOpCode(OpCodes.JUMP_ABSOLUTE);
                                    }
                                    if (forOffset >= 0 && forOffset < jumpOffset)
                                    {
                                        string iter = Expr(obj, code, DataStack, getIterOffset);
                                        code.GetNextInstruction();
                                        code.GetNextInstruction();
                                        string target = Expr(obj, code, DataStack);
                                        comp_for = " for " + target + " in " + iter;
                                    }
                                    int ifOffset = code.GetOffsetByOpCode(OpCodes.POP_JUMP_IF_FALSE);
                                    string ifExpr = "";
                                    if (ifOffset >= 0 && ifOffset <= jumpOffset)
                                    {
                                        ifExpr = " if " + Expr(obj, code, DataStack, ifOffset);
                                        code.GetNextInstruction();
                                    }
                                    int listAppendOffset = code.GetOffsetByOpCode(OpCodes.LIST_APPEND);
                                    string expr = Expr(obj, code, DataStack, listAppendOffset);
                                    code.GetNextInstruction();
                                    code.GetNextInstruction();

                                    string list_comp = "[" + expr + comp_for + ifExpr + "]";
                                    DataStack.Push(list_comp);
                                }
                                else
                                {
                                    string list = "[";
                                    for (int i = (int)code.Current.Argument - 1; i >= 0; i--)
                                    {
                                        list += DataStack.ElementAt(i) + (i > 0 ? ", " : "");
                                    }
                                    for (int i = 0; i < (int)code.Current.Argument; i++)
                                    {
                                        DataStack.Pop();
                                    }
                                    list += "]";
                                    DataStack.Push(list);
                                }
                                break;
                            }
                        case OpCodes.BUILD_MAP:
                            {
                                int forOffset = -1, getIterOffset = -1, jumpOffset = -1, setupLoopOffset = -1;
                                forOffset = code.GetOffsetByOpCode(OpCodes.FOR_ITER);
                                setupLoopOffset = code.GetOffsetByOpCode(OpCodes.SETUP_LOOP);
                                if (code.Current.Argument == 0 && (forOffset == -1 || (setupLoopOffset > 0 && forOffset > setupLoopOffset) || (forOffset - code.Current.Offset) > 15))
                                {
                                    DataStack.Push("{}");
                                }
                                else if (code.Current.Argument == 0)
                                {
                                    string comp_for = "";
                                    int genCount = -1;
                                    forOffset = code.GetOffsetByOpCode(OpCodes.FOR_ITER);
                                    if (forOffset > 0)
                                    {
                                        nextOpCode = code.PeekInstructionAtOffset((uint)forOffset);
                                        jumpOffset = (int)nextOpCode.JumpTarget - 3;
                                        genCount = code.CountSpecificOpCodes("FOR_ITER", jumpOffset);
                                    }
                                    else
                                    {
                                        break;
                                    }

                                    string comprehenstionGenerator = RecreateComprehensionGenerator(code, genCount, 0);


                                    if (forOffset >= 0 && forOffset < jumpOffset)
                                    {
                                        string iter = Expr(obj, code, DataStack, forOffset);
                                        code.SkipInstruction(1);
                                        string target = Expr(obj, code, DataStack);
                                        comp_for = " for " + target + " in " + iter;
                                    }
                                    int ifOffset = code.GetOffsetByOpCode(OpCodes.POP_JUMP_IF_FALSE);
                                    string ifExpr = "";
                                    if (ifOffset >= 0 && ifOffset <= jumpOffset)
                                    {
                                        ifExpr = " if " + Expr(obj, code, DataStack, ifOffset);
                                        code.GetNextInstruction();
                                    }
                                    int mapAddOffset = code.GetOffsetByOpCode(OpCodes.MAP_ADD);
                                    string key = Expr(obj, code, DataStack, mapAddOffset);
                                    string value = Expr(obj, code, DataStack, mapAddOffset);
                                    code.GetNextInstruction();
                                    code.GetNextInstruction();

                                    string map_comp = "{" + key + ":" + value + comp_for + ifExpr + "}";
                                    DataStack.Push(map_comp);
                                }
                                else
                                {
                                    uint count = code.Current.Argument;
                                    string mapResult = "{";
                                    while (count-- > 0)
                                    {
                                        string expr = Expr(obj, code, DataStack);
                                        mapResult += (mapResult == "{" ? " " : ", ") + expr;
                                    }
                                    mapResult += " }";
                                    DataStack.Push(mapResult);
                                }

                                break;
                            }
                        case OpCodes.BUILD_TUPLE:
                            {
                                // TODO: Check if Argument == 0 then it's generator
                                int forOffset = -1, getIterOffset = -1, jumpOffset = -1, setupLoopOffset = -1;
                                forOffset = code.GetOffsetByOpCode(OpCodes.FOR_ITER);
                                setupLoopOffset = code.GetOffsetByOpCode(OpCodes.SETUP_LOOP);
                                if (code.Current.Argument == 0 && (forOffset == -1 || (setupLoopOffset > 0 && forOffset > setupLoopOffset) || (forOffset - code.Current.Offset) > 15))
                                {
                                    DataStack.Push("()");
                                }
                                else if (code.Current.Argument == 0)
                                {
                                    // TODO: tuple generator code goes here
                                    throw new ArgumentException("tuple generator is not implemented yet");
                                }
                                else
                                {
                                    string tuple = "(";
                                    for (int i = (int)code.Current.Argument - 1; i >= 0; i--)
                                    {
                                        tuple += DataStack.ElementAt(i) + (i > 0 ? ", " : "");
                                    }
                                    tuple += ")";
                                    for (int i = 0; i < (int)code.Current.Argument; i++)
                                    {
                                        DataStack.Pop();
                                    }
                                    DataStack.Push(tuple);
                                }
                                break;
                            }
                        case OpCodes.BUILD_SET:
                            {
                                // TODO: Check if Argument == 0 then it's generator
                                int forOffset = -1, getIterOffset = -1, jumpOffset = -1, setupLoopOffset = -1;
                                forOffset = code.GetOffsetByOpCode(OpCodes.FOR_ITER);
                                setupLoopOffset = code.GetOffsetByOpCode(OpCodes.SETUP_LOOP);
                                if (code.Current.Argument == 0 && (forOffset == -1 || (setupLoopOffset > 0 && forOffset > setupLoopOffset) || (forOffset - code.Current.Offset) > 15))
                                {
                                    DataStack.Push("()");
                                }
                                else if (code.Current.Argument == 0)
                                {
                                    // TODO: set generator code goes here
                                    throw new ArgumentException("set generator is not implemented yet");
                                }
                                else
                                {
                                    string set = "{";
                                    for (int i = 0; i < code.Current.Argument; i++)
                                    {
                                        string value = DataStack.Pop();
                                        set += (set == "{") ? value : ", " + value;
                                    }
                                    set += "}";
                                    DataStack.Push(set);
                                }
                                break;
                            }
                        case OpCodes.LOAD_ATTR:
                            DataStack.Push(DataStack.Pop() + "." + code.Current.Name);
                            break;
                        case OpCodes.COMPARE_OP:
                            {
                                string rVal = DataStack.Pop();
                                string lVal = DataStack.Pop();
                                if ((rVal.Contains(" - ") || rVal.Contains(" + ")) && !rVal.Contains("("))
                                    rVal = "(" + rVal + ")";
                                if ((lVal.Contains(" - ") || lVal.Contains(" + ")) && !lVal.Contains("("))
                                    lVal = "(" + lVal + ")";
                                DataStack.Push(String.Format("{0} {1} {2}", lVal, code.Current.CompareOperator, rVal));
                            }
                            break;
                        case OpCodes.IMPORT_NAME:
                            {
                                DataStack.Pop();
                                DataStack.Pop();

                                string moduleName = code.Current.Name;
                                OpCode peekDecoder = code.PeekNextInstruction();
                                if (peekDecoder.OpCodeID == OpCodes.IMPORT_STAR)
                                {
                                    code.SkipInstruction();
                                    // Decode it as "from moduleName import *
                                    return String.Format("from {0} import *", moduleName);
                                }
                                else if (peekDecoder.OpCodeID == OpCodes.IMPORT_FROM)
                                {
                                    // Decode it as "from moduleName import '*' | (name as asname,)*
                                    nextOpCode = code.PeekPrevInstruction();
                                    code.SkipInstruction();
                                    PythonObject names = obj.Consts[(int)nextOpCode.Argument];
                                    if (names.ClassName == "Py_Tuple")
                                    {
                                        string output = String.Format("from {0} import ", moduleName);
                                        for (int idx = 0; idx < names.Count; idx++)
                                        {
                                            string name = (string)(names.Value as List<PythonObject>)[idx];
                                            string asName = Identifier(obj, code, DataStack);
                                            if (name != asName)
                                            {
                                                output += (idx > 0 ? ", " : "") + String.Format("{0} as {1}", name, asName);
                                            }
                                            else
                                            {
                                                output += (idx > 0 ? ", " : "") + name;
                                            }
                                        }
                                        code.GetNextInstruction();
                                        return output;
                                    }
                                    else
                                    {
                                        string name = Expr(obj, code, DataStack, code.GetOffsetByOpCodeName("STORE_") + 3);
                                        if (names != name)
                                        {
                                            code.GetNextInstruction();
                                            return String.Format("from {0} import {1} as {2}", moduleName, names, name);
                                        }
                                        else
                                        {
                                            code.GetNextInstruction();
                                            return String.Format("from {0} import {1}", moduleName, names);
                                        }
                                    }
                                }
                                else
                                {
                                    // Decode it as import moduleName
                                    if (peekDecoder.OpCodeID == OpCodes.LOAD_ATTR)
                                        code.SkipInstruction();
                                    string name = Identifier(obj, code, DataStack);
                                    if (name != moduleName)
                                    {
                                        return String.Format("import {0} as {1}", moduleName, name);
                                    }
                                    else
                                    {
                                        return String.Format("import {0}", moduleName);
                                    }
                                }
                            }
                        case OpCodes.IMPORT_FROM:
                            break;
                        case OpCodes.JUMP_FORWARD:
                            return ""; //MoveBack(code);
                        case OpCodes.JUMP_IF_FALSE_OR_POP:
                        case OpCodes.JUMP_IF_TRUE_OR_POP:
                            {
                                uint jumpTarget = code.Current.JumpTarget;
                                int targetIdx = code.GetIndexByOffset((int)jumpTarget);
                                string resultString = DataStack.Pop();
                                List<int> ifsOffs = new List<int>();

                                for (int idx = code.CurrentInstructionIndex + 1; idx < targetIdx; idx++)
                                {
                                    OpCode peekOp = code.PeekInstructionAt(idx);
                                    if (peekOp.OpCodeID == OpCodes.JUMP_IF_FALSE_OR_POP || peekOp.OpCodeID == OpCodes.JUMP_IF_TRUE_OR_POP)
                                        ifsOffs.Add(peekOp.Offset);
                                }

                                if (code.Current.OpCodeID == OpCodes.JUMP_IF_FALSE_OR_POP)
                                    resultString += " and ";
                                else
                                    resultString += " or ";


                                foreach (int offset in ifsOffs)
                                {
                                    resultString += Expr(obj, code, DataStack, offset);
                                    code.GetNextInstruction();
                                    if (code.Current.OpCodeID == OpCodes.JUMP_IF_FALSE_OR_POP)
                                        resultString += " and ";
                                    else
                                        resultString += " or ";
                                }
                                int jumpForward = code.GetOffsetByOpCode(OpCodes.JUMP_FORWARD);
                                if (jumpForward < jumpTarget && code.PeekInstructionAtOffset((uint)jumpForward + 3).OpCodeID == OpCodes.ROT_TWO && code.PeekInstructionAtOffset((uint)jumpForward + 4).OpCodeID == OpCodes.POP_TOP)
                                {
                                    resultString += Expr(obj, code, DataStack, (int)jumpForward);
                                    code.SkipInstruction(3);
                                }
                                else
                                {
                                    resultString += Expr(obj, code, DataStack, (int)jumpTarget);
                                }

                                DataStack.Push(resultString);
                                break;
                            }
                        case OpCodes.JUMP_ABSOLUTE:
                            break;
                        case OpCodes.POP_JUMP_IF_FALSE:
                            {
                                PycResult result = new PycResult();
                                DecompileIfExpression(obj, code, DataStack, result);
                                if (result.HasResult)
                                    return result.ToString();
                            }
                            break;
                        case OpCodes.POP_JUMP_IF_TRUE:
                            return MoveBack(code);
                        case OpCodes.LOAD_GLOBAL:
                            DataStack.Push(code.Current.Name);
                            break;
                        case OpCodes.CONTINUE_LOOP:
                            // TODO: Do I have to pop stuff from stack?
                            return "continue";
                        case OpCodes.SETUP_LOOP:
                            return MoveBack(code); // Handled in Stmts
                        case OpCodes.SETUP_EXCEPT:
                            return MoveBack(code); // Handled in Stmts
                        case OpCodes.SETUP_FINALLY:
                            return MoveBack(code); // Handled in Stmts
                        case OpCodes.LOAD_FAST:
                            DataStack.Push(code.Current.LocalName);
                            break;
                        case OpCodes.STORE_FAST:
                            {
                                if (DataStack.Count == 0 || code.PeekPrevInstruction().OpCodeID == OpCodes.FOR_ITER)
                                    return code.Current.LocalName;
                                string expr = code.Current.LocalName + " = " + DataStack.Pop();
                                if (code.PeekNextInstruction().OpCodeID == OpCodes.DUP_TOP)
                                {
                                    code.SkipInstruction();
                                    DataStack.Push(expr);
                                    break;
                                }
                                else
                                {
                                    return expr;
                                }
                            }
                        case OpCodes.DELETE_FAST:
                            return "del " + code.Current.LocalName;
                        case OpCodes.RAISE_VARARGS:
                            {
                                string raiseValue = "";
                                if (code.Current.Argument == 3)
                                    raiseValue = "," + DataStack.Pop();
                                if (code.Current.Argument >= 2)
                                    raiseValue = "," + DataStack.Pop() + raiseValue;
                                if (code.Current.Argument >= 1)
                                    raiseValue = DataStack.Pop() + raiseValue;

                                return "raise " + raiseValue;
                            }
                        case OpCodes.CALL_FUNCTION:
                            {
                                uint na = code.Current.Argument & 0xFF;
                                uint nk = (code.Current.Argument >> 8) & 0xFF;
                                uint n = na + nk * 2;
                                if (n > DataStack.Count)
                                    throw new ArgumentException(String.Format(@"ERROR: CALL_FUNCTION expects {0} params that bigger than stack size {1}", n, DataStack.Count));
                                string pars = "";
                                for (int i = 0; i < nk; i++)
                                {
                                    string value = DataStack.Pop();
                                    string key  = DataStack.Pop();
                                    pars = key.Trim('\'') + " = " + value + (pars != "" ? ", " : "") + pars;
                                }
                                for (int i = 0; i < na; i++)
                                {
                                    pars = DataStack.Pop() + (pars != "" ? ", " : "") + pars;
                                }
                                string functionName = DataStack.Pop();
                                DataStack.Push(functionName + "(" + pars + ")");
                                break;
                            }
                        case OpCodes.MAKE_FUNCTION:
                            return MoveBack(code); // Handled in Stmts
                        case OpCodes.BUILD_SLICE:
                            {
                                string step = "";
                                if (code.Current.Argument == 3)
                                    step = DataStack.Pop();
                                string endIndex = DataStack.Pop();
                                string startIndex = DataStack.Pop();

                                if (step == "None")
                                    step = "";
                                if (endIndex == "None")
                                    endIndex = "";
                                if (startIndex == "None")
                                    startIndex = "";

                                if (code.Current.Argument == 3)
                                {
                                    DataStack.Push(String.Format("{0}:{1}:{2}", startIndex, endIndex, step));
                                }
                                else
                                {
                                    DataStack.Push(String.Format("{0}:{1}", startIndex, endIndex));
                                }
                            }
                            break;
                        case OpCodes.MAKE_CLOSURE:
                            return MoveBack(code);
                        case OpCodes.LOAD_CLOSURE:
                            DataStack.Push(code.Current.FreeName);
                            break;
                        case OpCodes.LOAD_DEREF:
                            DataStack.Push(code.Current.FreeName);
                            break;
                        case OpCodes.STORE_DEREF:
                            {
                                if (DataStack.Count == 0 || code.PeekPrevInstruction().OpCodeID == OpCodes.FOR_ITER)
                                    return code.Current.FreeName;
                                string expr = code.Current.FreeName + " = " + DataStack.Pop();
                                if (code.PeekNextInstruction().OpCodeID == OpCodes.DUP_TOP)
                                {
                                    code.SkipInstruction();
                                    DataStack.Push(expr);
                                    break;
                                }
                                else
                                {
                                    return expr;
                                }
                            }
                        case OpCodes.CALL_FUNCTION_VAR:
                            {
                                uint na = code.Current.Argument & 0xFF;
                                uint nk = (code.Current.Argument >> 8) & 0xFF;
                                uint n = na + nk * 2 + 1;
                                if (n > DataStack.Count)
                                    throw new ArgumentException(String.Format(@"ERROR: CALL_FUNCTION expects {0} params that bigger than stack size {1}", code.Current.Argument, DataStack.Count));
                                string starArguments = DataStack.Pop();
                                string pars = "";
                                for (int i = 0; i < nk; i++)
                                {
                                    string value = DataStack.Pop();
                                    string key = DataStack.Pop();
                                    pars = key.Trim('\'') + " = " + value + (pars != "" ? ", " : "") + pars;
                                }
                                for (int i = 0; i < na; i++)
                                {
                                    pars = DataStack.Pop() + (pars != "" ? ", " : "") + pars;
                                }
                                string functionName = DataStack.Pop();
                                DataStack.Push(functionName + "(" + pars + (pars != "" ? ", " : "") + "*" + starArguments + ")");
                            }
                            break;
                        case OpCodes.CALL_FUNCTION_KW:
                            {
                                uint na = code.Current.Argument & 0xFF;
                                uint nk = (code.Current.Argument >> 8) & 0xFF;
                                uint n = na + nk * 2 + 1;
                                if (n > DataStack.Count)
                                    throw new ArgumentException(String.Format(@"ERROR: CALL_FUNCTION expects {0} params that bigger than stack size {1}", code.Current.Argument, DataStack.Count));
                                string pars = "";
                                string keywordArguments = DataStack.Pop();
                                for (int i = 0; i < nk; i++)
                                {
                                    string value = DataStack.Pop();
                                    string key = DataStack.Pop();
                                    pars = key.Trim('\'') + " = " + value + (pars != "" ? ", " : "") + pars;
                                }
                                for (int i = 0; i < na; i++)
                                {
                                    pars = DataStack.Pop() + (pars != "" ? ", " : "") + pars;
                                }
                                string functionName = DataStack.Pop();
                                DataStack.Push(functionName + "(" + pars + (pars != "" ? ", " : "") + "**" + keywordArguments + ")");
                            }
                            break;
                        case OpCodes.CALL_FUNCTION_VAR_KW:
                            {
                                uint na = code.Current.Argument & 0xFF;
                                uint nk = (code.Current.Argument >> 8) & 0xFF;
                                uint n = na + nk * 2 + 2;
                                string keywordArguments = DataStack.Pop();
                                string starArguments = DataStack.Pop();
                                if (n > DataStack.Count)
                                    throw new ArgumentException(String.Format(@"ERROR: CALL_FUNCTION expects {0} params that bigger than stack size {1}", code.Current.Argument, DataStack.Count));
                                string pars = "";
                                for (int i = 0; i < nk; i++)
                                {
                                    string value = DataStack.Pop();
                                    string key = DataStack.Pop();
                                    pars = key.Trim('\'') + " = " + value + (pars != "" ? ", " : "") + pars;
                                }
                                for (int i = 0; i < na; i++)
                                {
                                    pars = DataStack.Pop() + (pars != "" ? ", " : "") + pars;
                                }
                                string functionName = DataStack.Pop();
                                DataStack.Push(functionName + "(" + pars + (pars != "" ? ", " : "") + " *" + starArguments + ", **" + keywordArguments + ")");
                            }
                            break;
                        case OpCodes.SETUP_WITH:
                            return "";
                        case OpCodes.EXTENDED_ARG:
                            return "";
                        case OpCodes.SET_ADD:
                            return "";
                        case OpCodes.MAP_ADD:
                            return "";
                        default:
                            return MoveBack(code);
                    }
                }
                catch (Exception ex)
                {
                    return String.Format("EXCEPTION for OpCode {0} ({1}) at offset {2} : {3}\n\n", code.Current.InstructionName, code.Current.Argument, code.Current.Offset, ex.Message);
                }
            }

            return "";
        }

        private static string RecreateComprehensionGenerator(OpCodes code, int genCount, int currentIndex = 0, bool isCompFunc = true)
        {
            if (isCompFunc)
            {
                if (currentIndex == 0)
                {

                }
                else
                {
                }
            }
            return "";
        }

        public static string MoveBack(OpCodes code)
        {
            if (code.CurrentInstructionIndex > 0)
                code.CurrentInstructionIndex--;
            return "";
        }

        public static string Identifier(PythonCodeObject obj, OpCodes code, Stack<string> DataStack)
        {
            if (obj == null)
                return "";

            while (code.HasInstructionsToProcess)
            {
                try
                {
                    code.GetNextInstruction();
                    string v1 = "", v2 = "", v3 = "";

                    switch (code.Current.OpCodeID)
                    {
                        case OpCodes.BINARY_SUBSCR:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + "[" + v1 + "]";
                            return v1;
                        case OpCodes.SLICE:
                            v1 = DataStack.Pop();
                            v1 = v1 + "[:]";
                            return v1;
                        case OpCodes.SLICE1:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + "[" + v1 + ":]";
                            return v1;
                        case OpCodes.SLICE2:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = v2 + "[:" + v1 + "]";
                            return v1;
                        case OpCodes.SLICE3:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v3 = DataStack.Pop();
                            v1 = v3 + "[" + v1 + ":" + v2 + "]";
                            return v1;
                        case OpCodes.STORE_SLICE:
                            v1 = DataStack.Pop();
                            return v1 + "[:]";
                        case OpCodes.STORE_SLICE1:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            return v2 + "[" + v1 + ":]";
                        case OpCodes.STORE_SLICE2:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            return v2 + "[:" + v1 + "]";
                        case OpCodes.STORE_SLICE3:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v3 = DataStack.Pop();
                            return v3 + "[" + v1 + ":" + v2 + "]";
                        case OpCodes.STORE_SUBSCR:
                            v1 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            return v2 + "[" + v1 + "]";
                        case OpCodes.STORE_NAME:
                            return code.Current.Name;
                        case OpCodes.STORE_ATTR:
                            v1 = DataStack.Pop();
                            return v1 + "." + code.Current.Name;
                        case OpCodes.STORE_GLOBAL:
                            return code.Current.Name;
                        case OpCodes.LOAD_CONST:
                            DataStack.Push(code.Current.Constant);
                            break;
                        case OpCodes.LOAD_NAME:
                            DataStack.Push(code.Current.Name);
                            break;
                        case OpCodes.LOAD_ATTR:
                            v1 = DataStack.Pop();
                            v1 = v1 + "." + code.Current.Name;
                            DataStack.Push(v1);
                            break;
                        case OpCodes.LOAD_GLOBAL:
                            DataStack.Push(code.Current.Name);
                            break;
                        case OpCodes.LOAD_FAST:
                            DataStack.Push(code.Current.LocalName);
                            break;
                        case OpCodes.STORE_FAST:
                            return code.Current.LocalName;
                        case OpCodes.BUILD_SLICE:
                            if (code.Current.Argument == 3)
                                v3 = DataStack.Pop();
                            v2 = DataStack.Pop();
                            v1 = DataStack.Pop();

                            if (v3 == "None")
                                v3 = "";
                            if (v2 == "None")
                                v2 = "";
                            if (v1 == "None")
                                v1 = "";

                            if (code.Current.Argument == 3)
                            {
                                DataStack.Push(String.Format("{0}:{1}:{2}", v1, v2, v3));
                            }
                            else
                            {
                                DataStack.Push(String.Format("{0}:{1}", v1, v2));
                            }
                            break;
                        case OpCodes.LOAD_CLOSURE:
                            DataStack.Push(code.Current.FreeName);
                            break;
                        case OpCodes.LOAD_DEREF:
                            DataStack.Push(code.Current.FreeName);
                            break;
                        case OpCodes.STORE_DEREF:
                            return code.Current.FreeName;
                        default:
                            return DataStack.Pop();
                    }
                }
                catch (Exception ex)
                {
                    return String.Format("EXCEPTION for OpCode {0} ({1}) at offset {2} : {3}\n\n", code.Current.InstructionName, code.Current.Argument, code.Current.Offset, ex.Message);
                }
            }

            return DataStack.Pop();
        }

    }
}
