﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace PyjReader
{
    public class PycReader
    {
        public const int PyLong_MARSHAL_SHIFT = 15;
        public const int PyLong_MARSHAL_RATIO = 30 / PyLong_MARSHAL_SHIFT;
        public const int PyLong_MARSHAL_BASE = 0x8000;
        public const int PyLong_MARSHAL_MASK = 0x7FFF;

        public const char TypeNull = '0';
        public const char TypeNone = 'N';
        public const char TypeFalse = 'F';
        public const char TypeTrue = 'T';
        public const char TypeStopiter = 'S';
        public const char TypeEllipsis = '.';
        public const char TypeInt = 'i';
        public const char TypeInt64 = 'I';
        public const char TypeFloat = 'f';
        public const char TypeBinaryFloat = 'g';
        public const char TypeComplex = 'x';
        public const char TypeBinaryComplex = 'y';
        public const char TypeLong = 'l';
        public const char TypeString = 's';
        public const char TypeInterned = 't';
        public const char TypeStringRef = 'R';
        public const char TypeTuple = '(';
        public const char TypeList = '[';
        public const char TypeDict = '{';
        public const char TypeCode = 'c';
        public const char TypeUnicode = 'u';
        public const char TypeUnknown = '?';
        public const char TypeSet = '<';
        public const char TypeFrozenset = '>';
        public List<byte[]> Strings { get; set; }
        private BinaryReader m_rdr;

        public PycReader(string filePath)
        {
            Strings = new List<byte[]>();
            m_rdr = new BinaryReader(File.OpenRead(filePath));
            UInt16 marshalVersion = m_rdr.ReadUInt16();
            if (marshalVersion == 0x63)
            {
                m_rdr.BaseStream.Position = 0;
            }
            else
            {
                UInt16 tmp = m_rdr.ReadUInt16();
                UInt32 timeStamp = m_rdr.ReadUInt32();
            }

        }

        public PythonObject ReadObject()
        {
            PythonObject obj = null;
            char objectType = m_rdr.ReadChar();

            switch (objectType)
            {
                case TypeNull:
                    obj = new PythonObject
                              {
                                  ClassName = "Py_Null",
                                  Value = null
                              };
                    break;
                case TypeNone:
                    obj = new PythonObject
                              {
                                  ClassName = "Py_None",
                                  Value = null
                              };
                    break;
                case TypeStopiter:
                    obj = new PythonObject
                              {
                                  ClassName = "Py_StopIteration",
                                  Value = null
                              };
                    break;
                case TypeEllipsis:
                    obj = new PythonObject
                              {
                                  ClassName = "Py_Ellipsis",
                                  Value = null
                              };
                    break;
                case TypeFalse:
                    obj = new PythonObject
                              {
                                  ClassName = "Py_False",
                                  Value = false
                              };
                    break;
                case TypeTrue:
                    obj = new PythonObject
                              {
                                  ClassName = "Py_True",
                                  Value = true
                              };
                    break;
                case TypeInt:
                    obj = new PythonObject
                              {
                                  ClassName = "Py_Int",
                                  Value = m_rdr.ReadInt32()
                              };
                    break;
                case TypeInt64:
                    obj = new PythonObject
                              {
                                  ClassName = "Py_Long",
                                  Value = m_rdr.ReadInt64()
                              };
                    break;
                case TypeLong:
                    List<int> longValue = new List<int>();
                    Int32 n = m_rdr.ReadInt32();
                    bool negative = n < 0;
                    if (n == 0)
                    {
                        longValue.Add(0);
                    } 
                    else
                    {
                        Int32 d = 0;
                        int size = 1 + (Math.Abs(n) - 1) / PyLong_MARSHAL_RATIO;
                        int shorts_in_top_digit = 1 + (Math.Abs(n) - 1) % PyLong_MARSHAL_RATIO;
                        for(int idx = 0; idx < size - 1; idx++)
                        {
                            d = m_rdr.ReadInt16() + m_rdr.ReadInt16() << 15;
                            longValue.Add(d);
                        }
                        if (shorts_in_top_digit == 1)
                        {
                            longValue.Add((int)m_rdr.ReadInt16());
                        }
                        else if (shorts_in_top_digit == 2)
                        {
                            d = m_rdr.ReadInt16() + m_rdr.ReadInt16() << 15;
                            longValue.Add(d);
                        }
                    }
                    if (longValue.Count > 2)
                    {
                        // TODO: Accomodate negative sign.
                        obj = new PythonObject
                                  {
                                      ClassName = "Py_VeryLong",
                                      Value = longValue
                                  };
                    }
                    else
                    {
                        UInt64 tempLongValue = (UInt64)longValue[longValue.Count - 1];
                        for (int idx = longValue.Count - 2; idx >= 0 ; idx--)
                        {
                            tempLongValue = tempLongValue * (UInt64)(1 << 30) + (UInt64)longValue[idx];
                        }
                        Int64 resultLongValue = (negative ? -1 : 1) * (Int64)tempLongValue;
                        
                        obj = new PythonObject
                        {
                            ClassName = "Py_Long",
                            Value = resultLongValue
                        };
                    }
                    break;
                case TypeFloat:
                    obj = new PythonObject
                              {
                                  ClassName = "Py_Float",
                                  Value = ReadString()
                              };
                    break;
                case TypeBinaryFloat:
                    obj = new PythonObject
                              {
                                  ClassName = "Py_Float",
                                  Value = m_rdr.ReadDouble()
                              };
                    break;
                case TypeComplex:
                    obj = new PythonObject
                              {
                                  ClassName = "Py_Complex",
                                  Value = new List<string> {ConvertBytesToString(ReadString()), ConvertBytesToString(ReadString())}
                              };
                    break;
                case TypeBinaryComplex:
                    obj = new PythonObject
                              {
                                  ClassName = "Py_Complex",
                                  Value = new List<double> {m_rdr.ReadDouble(), m_rdr.ReadDouble()}
                              };
                    break;
                case TypeUnicode:
                    obj = new PythonObject
                              {
                                  ClassName = "Py_Unicode",
                                  Value = ReadString(m_rdr.ReadUInt32())
                              };
                    break;
                case TypeString:
                    obj = new PythonObject
                              {
                                  ClassName = "Py_String",
                                  Value = ReadString(m_rdr.ReadUInt32())
                              };
                    break;
                case TypeInterned:
                    byte[] value = ReadString(m_rdr.ReadUInt32());
                    obj = new PythonObject
                              {
                                  ClassName = "Py_String",
                                  Value = value
                              };
                    Strings.Add(value);
                    break;
                case TypeStringRef:
                    UInt32 listPos = m_rdr.ReadUInt32();
                    obj = new PythonObject
                              {
                                  ClassName = "Py_String",
                                  Value = (listPos < Strings.Count ? Strings[(int) listPos] : null)
                              };
                    break;
                case TypeTuple:
                    UInt32 nTuples = m_rdr.ReadUInt32();
                    List<PythonObject> tuples = new List<PythonObject>();
                    for (UInt32 currentIndex = 0; currentIndex < nTuples; currentIndex++ )
                    {
                        tuples.Add(ReadObject());
                    }
                    obj = new PythonObject
                              {
                                  ClassName = "Py_Tuple",
                                  Value = tuples
                              };
                    break;
                case TypeList:
                    UInt32 nListElements = m_rdr.ReadUInt32();
                    List<PythonObject> list = new List<PythonObject>();
                    for (UInt32 currentIndex = 0; currentIndex < nListElements; currentIndex++)
                    {
                        list.Add(ReadObject());
                    }
                    obj = new PythonObject
                              {
                                  ClassName = "Py_List",
                                  Value = list
                              };
                    break;
                case TypeDict:
                    Dictionary<PythonObject, PythonObject> dict = new Dictionary<PythonObject, PythonObject>();
                    while(true)
                    {
                        PythonObject key = ReadObject();
                        if (key.ClassName == "Py_Null")
                            break;
                        PythonObject dictValue = ReadObject();
                        dict[key] = dictValue;
                    }
                    obj = new PythonObject
                              {
                                  ClassName = "Py_Dict",
                                  Value = dict
                              };
                    break;
                case TypeSet:
                    UInt32 nSetElements = m_rdr.ReadUInt32();
                    List<PythonObject> set = new List<PythonObject>();
                    for (UInt32 currentIndex = 0; currentIndex < nSetElements; currentIndex++)
                    {
                        set.Add(ReadObject());
                    }
                    obj = new PythonObject
                              {
                                  ClassName = "Py_Set",
                                  Value = set
                              };
                    break;
                case TypeFrozenset:
                    UInt32 nFSetElements = m_rdr.ReadUInt32();
                    List<PythonObject> frozenSet = new List<PythonObject>();
                    for (UInt32 currentIndex = 0; currentIndex < nFSetElements; currentIndex++)
                    {
                        frozenSet.Add(ReadObject());
                    }
                    obj = new PythonObject
                              {
                                  ClassName = "Py_FrozenSet",
                                  Value = frozenSet
                              };
                    break;
                case TypeCode:
                    obj = ReadCodeObject();
                    break;
                default:
                    throw new ArgumentException("objectType", String.Format("Don't know how to handle object Type '{0}'", objectType));
            }

            return obj;
        }

        public static string ConvertBytesToString(byte[] bytes)
        {
            string str = "";
            if (bytes.Length == 1 && (bytes[0] == 13))
            {
                str = @"\n";
            }
            else if (bytes.Length == 2 && bytes[0] == 13 && bytes[1] == 10)
            {
                str = @"\r\n";
            }
            else
            {
                foreach (byte b in bytes)
                {
                    str += (char)b;
                }
            }

            return str;
        }

        private byte [] ReadString(uint? size = null)
        {
            int n = 0;
            n = (size == null) ? m_rdr.ReadChar() : (int)size.Value;
            return m_rdr.ReadBytes(n);
        }

        private PythonObject ReadCodeObject()
        {
            PythonCodeObject codeObject = new PythonCodeObject { ClassName = "Py_CodeObject" };

            codeObject.ArgCount = m_rdr.ReadUInt32();
            codeObject.NumLocals = m_rdr.ReadUInt32();
            codeObject.StackSize = m_rdr.ReadUInt32();
            codeObject.Flags = m_rdr.ReadUInt32();
            codeObject.Code = ReadObject();
            codeObject.Consts = ReadObject();
            codeObject.Names = ReadObject();
            codeObject.VarNames = ReadObject();
            codeObject.FreeVars = ReadObject();
            codeObject.CellVars = ReadObject();
            codeObject.FileName = ReadObject();
            codeObject.Name = ReadObject();
            codeObject.FirstLineNo = m_rdr.ReadUInt32();
            codeObject.Methods = new Dictionary<uint, PythonCodeObject>();
            UnpackLineNumbers(codeObject);


            return codeObject;
        }

        private void UnpackLineNumbers(PythonCodeObject codeObject)
        {
            codeObject.LineNoTab = new List<int>(codeObject.Code.Length);
            byte[] lineno = ReadObject();
            int bytePos = 0, currentBytePos = 0;
            int linePos = (int)codeObject.FirstLineNo;
            for (int idx = 0; idx < lineno.Length; idx += 2)
            {
                bytePos += lineno[idx];
                while(currentBytePos < bytePos)
                {
                    codeObject.LineNoTab.Add(linePos);
                    currentBytePos++;
                }
                linePos += lineno[idx + 1];
            }

            while (currentBytePos < codeObject.Code.Length)
            {
                codeObject.LineNoTab.Add(linePos);
                currentBytePos++;
            }
        }

        public string DumpObject(PythonObject obj, int level = 0)
        {
            string result = "";
            for (int idx = 0; idx < level; idx++ )
                result += "    ";
            switch (obj.ClassName)
            {
                case "Py_Null":
                    result += "Py_NULL\n";
                    break;
                case "Py_None":
                    result += "Py_None\n";
                    break;
                case "Py_StopIteration":
                    result += String.Format("Py_StopIteration, Value = {0}\n", obj.Value);
                    break;
                case "Py_Ellipsis":
                    result += String.Format("Py_Ellipsis, Value = {0}\n", obj.Value);
                    break;
                case "Py_False":
                    result += String.Format("Py_False\n");
                    break;
                case "Py_True":
                    result += String.Format("Py_True\n");
                    break;
                case "Py_Int":
                    result += String.Format("Py_Int, Value = {0}\n", (int)(obj.Value));
                    break;
                case "Py_Long":
                    result += String.Format("Py_Long, Value = {0}\n", obj.Value);
                    break;
                case "Py_VeryLong":
                    result += String.Format("Py_VeryLong, Value = {0}\n", obj.Value);
                    break;
                case "Py_Float":
                    result += String.Format("Py_Float, Value = {0}\n", obj.Value);
                    break;
                case "Py_Complex":
                    result += String.Format("Py_Complex, Value = {0}\n", obj.Value);
                    break;
                case "Py_Unicode":
                    result += String.Format("Py_Unicode, Value = {0}\n", obj.Value);
                    break;
                case "Py_String":
                    result += String.Format("Py_String, Value = {0}\n", ConvertBytesToString((byte[])obj.Value));
                    break;
                case "Py_Tuple":
                    result += String.Format("Py_Typle:\n");
                    List<PythonObject> tupleData = obj.Value as List<PythonObject>;
                    foreach (PythonObject item in tupleData)
                    {
                        result += DumpObject(item, level + 1);
                    }
                    break;
                case "Py_List":
                    result += String.Format("Py_List:\n");
                    List<PythonObject> listData = obj.Value as List<PythonObject>;
                    foreach (PythonObject item in listData)
                    {
                        result += DumpObject(item, level + 1);
                    }
                    break;
                case "Py_Dict":
                    result += String.Format("Py_Dict:\n");
                    Dictionary<PythonObject, PythonObject> dictData = obj.Value as Dictionary<PythonObject, PythonObject>;
                    foreach (KeyValuePair<PythonObject, PythonObject> pair in dictData)
                    {
                        result += String.Format("Dict Key:\n");
                        result += DumpObject(pair.Key, level + 1);
                        result += String.Format("Dict Value:\n");
                        result += DumpObject(pair.Value, level + 1);
                    }
                    break;
                case "Py_Set":
                    result += String.Format("Py_Set:\n");
                    List<PythonObject> setData = obj.Value as List<PythonObject>;
                    foreach (PythonObject item in setData)
                    {
                        result += DumpObject(item, level + 1);
                    }
                    break;
                case "Py_FrozenSet":
                    result += String.Format("Py_FrozenSet:\n");
                    List<PythonObject> fsData = obj.Value as List<PythonObject>;
                    foreach (PythonObject item in fsData)
                    {
                        result += DumpObject(item, level + 1);
                    }
                    break;
                case "Py_CodeObject":
                    result += String.Format("Py_CodeObject\n");
                    PythonCodeObject codeObject = obj as PythonCodeObject;
                    if (codeObject == null)
                        break;
                    string pars = "";
                    if (codeObject.ArgCount > 0)
                    {
                        for(int idx=0; idx < codeObject.ArgCount; idx++)
                        {
                            pars += pars == "" ? (string)codeObject.VarNames[idx] : ", " + codeObject.VarNames[idx];
                        }
                        
                    }
                    result += String.Format("Code Object: {0} ({1})\n", codeObject.Name, pars);
                    result += String.Format("ArgCount = {0}\n", codeObject.ArgCount);
                    result += String.Format("NumLocals {0}\n", codeObject.NumLocals);
                    result += String.Format("StackSize {0}\n", codeObject.StackSize);
                    result += String.Format("Flags {0}\n", codeObject.Flags);
                    result += String.Format("Consts:\n"); 
                    foreach (PythonObject item in codeObject.Consts)
	                {
                        result += DumpObject(item, level + 1);
                    }
                    result += String.Format("Names:\n");
                    foreach (PythonObject item in codeObject.Names)
	                {
                        result += DumpObject(item, level + 1);
                    }
                    result += String.Format("VarNames:\n");
                    foreach (PythonObject item in codeObject.VarNames)
	                {
                        result += DumpObject(item, level + 1);
                    }
                    result += String.Format("FreeVars:\n");
                    foreach (PythonObject item in codeObject.FreeVars)
	                {
                        result += DumpObject(item, level + 1);
                    }
                    result += String.Format("CellVars:\n");
                    foreach (PythonObject item in codeObject.CellVars)
	                {
                        result += DumpObject(item, level + 1);
                    }
                    result += String.Format("FileName = {0}\n", codeObject.FileName);
                    result += String.Format("Name = {0}\n", codeObject.Name);
                    result += String.Format("FirstLineNo = {0}\n", codeObject.FirstLineNo);
                    result += String.Format("Code:\n{0}\n", PycDisassembler.Disassembler(codeObject));
                    break;
                default:
                    result += String.Format("No such type {0}. Value = {1}\n", obj.ClassName, obj.Value);
                    break;
            }
            return result;
        }

        private static string GetMethodParametersString(PythonCodeObject codeObject)
        {
            string pars = "";
            if (codeObject.ArgCount > 0)
            {
                for (int idx = 0; idx < codeObject.ArgCount; idx++)
                {
                    pars += pars == "" ? (string)codeObject.VarNames[idx] : ", " + codeObject.VarNames[idx];
                }
            }
            return pars;
        }

    }
}
