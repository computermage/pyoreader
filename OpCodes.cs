﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PyjReader
{
    public class OpCodes
    {
        public const byte STOP_CODE = 0;

        public const byte POP_TOP = 1;
        public const byte ROT_TWO = 2;
        public const byte ROT_THREE = 3;
        public const byte DUP_TOP = 4;
        public const byte ROT_FOUR = 5;
        public const byte NOP = 9;

        public const byte UNARY_POSITIVE = 10;
        public const byte UNARY_NEGATIVE = 11;
        public const byte UNARY_NOT = 12;
        public const byte UNARY_CONVERT = 13;

        public const byte UNARY_INVERT = 15;

        public const byte BINARY_POWER = 19;

        public const byte BINARY_MULTIPLY = 20;
        public const byte BINARY_DIVIDE = 21;
        public const byte BINARY_MODULO = 22;
        public const byte BINARY_ADD = 23;
        public const byte BINARY_SUBTRACT = 24;
        public const byte BINARY_SUBSCR = 25;
        public const byte BINARY_FLOOR_DIVIDE = 26;
        public const byte BINARY_TRUE_DIVIDE = 27;
        public const byte INPLACE_FLOOR_DIVIDE = 28;
        public const byte INPLACE_TRUE_DIVIDE = 29;

        public const byte SLICE = 30;
        public const byte SLICE1 = 31;
        public const byte SLICE2 = 32;
        public const byte SLICE3 = 33;

        public const byte STORE_SLICE = 40;
        public const byte STORE_SLICE1 = 41;
        public const byte STORE_SLICE2 = 42;
        public const byte STORE_SLICE3 = 43;

        public const byte DELETE_SLICE = 50;
        public const byte DELETE_SLICE1 = 51;
        public const byte DELETE_SLICE2 = 52;
        public const byte DELETE_SLICE3 = 53;

        public const byte STORE_MAP = 54;
        public const byte INPLACE_ADD = 55;
        public const byte INPLACE_SUBTRACT = 56;
        public const byte INPLACE_MULTIPLY = 57;
        public const byte INPLACE_DIVIDE = 58;
        public const byte INPLACE_MODULO = 59;
        public const byte STORE_SUBSCR = 60;
        public const byte DELETE_SUBSCR = 61;

        public const byte BINARY_LSHIFT = 62;
        public const byte BINARY_RSHIFT = 63;
        public const byte BINARY_AND = 64;
        public const byte BINARY_XOR = 65;
        public const byte BINARY_OR = 66;
        public const byte INPLACE_POWER = 67;
        public const byte GET_ITER = 68;

        public const byte PRINT_EXPR = 70;
        public const byte PRINT_ITEM = 71;
        public const byte PRINT_NEWLINE = 72;
        public const byte PRINT_ITEM_TO = 73;
        public const byte PRINT_NEWLINE_TO = 74;
        public const byte INPLACE_LSHIFT = 75;
        public const byte INPLACE_RSHIFT = 76;
        public const byte INPLACE_AND = 77;
        public const byte INPLACE_XOR = 78;
        public const byte INPLACE_OR = 79;
        public const byte BREAK_LOOP = 80;
        public const byte WITH_CLEANUP = 81;
        public const byte LOAD_LOCALS = 82;
        public const byte RETURN_VALUE = 83;
        public const byte IMPORT_STAR = 84;
        public const byte EXEC_STMT = 85;
        public const byte YIELD_VALUE = 86;
        public const byte POP_BLOCK = 87;
        public const byte END_FINALLY = 88;
        public const byte BUILD_CLASS = 89;

        public const byte HAVE_ARGUMENT = 90;	/* Opcodes from here have an argument: */

        public const byte STORE_NAME = 90;	/* Index in name list */
        public const byte DELETE_NAME = 91;	/* "" */
        public const byte UNPACK_SEQUENCE = 92;	/* Number of sequence items */
        public const byte FOR_ITER = 93;
        public const byte LIST_APPEND = 94;

        public const byte STORE_ATTR = 95;	/* Index in name list */
        public const byte DELETE_ATTR = 96;	/* "" */
        public const byte STORE_GLOBAL = 97;	/* "" */
        public const byte DELETE_GLOBAL = 98;	/* "" */
        public const byte DUP_TOPX = 99;	/* number of items to duplicate */
        public const byte LOAD_CONST = 100;	/* Index in const list */
        public const byte LOAD_NAME = 101;	/* Index in name list */
        public const byte BUILD_TUPLE = 102;	/* Number of tuple items */
        public const byte BUILD_LIST = 103;	/* Number of list items */
        public const byte BUILD_SET = 104;     /* Number of set items */
        public const byte BUILD_MAP = 105;	/* Always zero for now */
        public const byte LOAD_ATTR = 106;	/* Index in name list */
        public const byte COMPARE_OP = 107;	/* Comparison operator */
        public const byte IMPORT_NAME = 108;	/* Index in name list */
        public const byte IMPORT_FROM = 109;	/* Index in name list */
        public const byte JUMP_FORWARD = 110;	/* Number of bytes to skip */

        public const byte JUMP_IF_FALSE_OR_POP = 111; /* Target byte offset from beginning of code */
        public const byte JUMP_IF_TRUE_OR_POP = 112;	/* "" */
        public const byte JUMP_ABSOLUTE = 113;	/* "" */
        public const byte POP_JUMP_IF_FALSE = 114;	/* "" */
        public const byte POP_JUMP_IF_TRUE = 115;	/* "" */

        public const byte LOAD_GLOBAL = 116;	/* Index in name list */

        public const byte CONTINUE_LOOP = 119;	/* Start of loop (absolute) */
        public const byte SETUP_LOOP = 120;	/* Target address (relative) */
        public const byte SETUP_EXCEPT = 121;	/* "" */
        public const byte SETUP_FINALLY = 122;	/* "" */

        public const byte LOAD_FAST = 124;	/* Local variable number */
        public const byte STORE_FAST = 125;	/* Local variable number */
        public const byte DELETE_FAST = 126;	/* Local variable number */

        public const byte RAISE_VARARGS = 130;	/* Number of raise arguments (1, 2 or 3) */
        /* CALL_FUNCTION_XXX opcodes defined below depend on this definition */
        public const byte CALL_FUNCTION = 131;	/* #args + (#kwargs<<8) */
        public const byte MAKE_FUNCTION = 132;	/* #defaults */
        public const byte BUILD_SLICE = 133;	/* Number of items */

        public const byte MAKE_CLOSURE = 134;     /* #free vars */
        public const byte LOAD_CLOSURE = 135;     /* Load free variable from closure */
        public const byte LOAD_DEREF = 136;     /* Load and dereference from closure cell */
        public const byte STORE_DEREF = 137;     /* Store into cell */

        /* The next 3 opcodes must be contiguous and satisfy
           (CALL_FUNCTION_VAR - CALL_FUNCTION) & 3 == 1  */
        public const byte CALL_FUNCTION_VAR = 140;	/* #args + (#kwargs<<8) */
        public const byte CALL_FUNCTION_KW = 141;	/* #args + (#kwargs<<8) */
        public const byte CALL_FUNCTION_VAR_KW = 142;	/* #args + (#kwargs<<8) */

        public const byte SETUP_WITH = 143;

        /* Support for opargs more than 16 bits long */
        public const byte EXTENDED_ARG = 145;

        public const byte SET_ADD = 146;
        public const byte MAP_ADD = 147;


        enum cmp_op
        {
            PyCmp_LT, PyCmp_LE, PyCmp_EQ, PyCmp_NE, PyCmp_GT, PyCmp_GE,
            PyCmp_IN, PyCmp_NOT_IN, PyCmp_IS, PyCmp_IS_NOT, PyCmp_EXC_MATCH, PyCmp_BAD
        };

        public static Dictionary<byte, OpCode> OpCodeList = new Dictionary<byte, OpCode>
        {
            {STOP_CODE, new OpCode(STOP_CODE, "STOP_CODE")},
            {POP_TOP, new OpCode(POP_TOP, "POP_TOP")},
            {ROT_TWO, new OpCode(ROT_TWO, "ROT_TWO")},
            {ROT_THREE, new OpCode(ROT_THREE, "ROT_THREE")},
            {DUP_TOP, new OpCode(DUP_TOP, "DUP_TOP")},
            {ROT_FOUR, new OpCode(ROT_FOUR, "ROT_FOUR")},
            {NOP, new OpCode(NOP, "NOP")},
            {UNARY_POSITIVE, new OpCode(UNARY_POSITIVE, "UNARY_POSITIVE")},
            {UNARY_NEGATIVE, new OpCode(UNARY_NEGATIVE, "UNARY_NEGATIVE")},
            {UNARY_NOT, new OpCode(UNARY_NOT, "UNARY_NOT")},
            {UNARY_CONVERT, new OpCode(UNARY_CONVERT, "UNARY_CONVERT")},
            {UNARY_INVERT, new OpCode(UNARY_INVERT, "UNARY_INVERT")},
            {BINARY_POWER, new OpCode(BINARY_POWER, "BINARY_POWER")},
            {BINARY_MULTIPLY, new OpCode(BINARY_MULTIPLY, "BINARY_MULTIPLY")},
            {BINARY_DIVIDE, new OpCode(BINARY_DIVIDE, "BINARY_DIVIDE")},
            {BINARY_MODULO, new OpCode(BINARY_MODULO, "BINARY_MODULO")},
            {BINARY_ADD, new OpCode(BINARY_ADD, "BINARY_ADD")},
            {BINARY_SUBTRACT, new OpCode(BINARY_SUBTRACT, "BINARY_SUBTRACT")},
            {BINARY_SUBSCR, new OpCode(BINARY_SUBSCR, "BINARY_SUBSCR")},
            {BINARY_FLOOR_DIVIDE, new OpCode(BINARY_FLOOR_DIVIDE, "BINARY_FLOOR_DIVIDE")},
            {BINARY_TRUE_DIVIDE, new OpCode(BINARY_TRUE_DIVIDE, "BINARY_TRUE_DIVIDE")},
            {INPLACE_FLOOR_DIVIDE, new OpCode(INPLACE_FLOOR_DIVIDE, "INPLACE_FLOOR_DIVIDE")},
            {INPLACE_TRUE_DIVIDE, new OpCode(INPLACE_TRUE_DIVIDE, "INPLACE_TRUE_DIVIDE")},
            {SLICE, new OpCode(SLICE, "SLICE")},
            {SLICE1, new OpCode(SLICE1, "SLICE1")},
            {SLICE2, new OpCode(SLICE2, "SLICE2")},
            {SLICE3, new OpCode(SLICE3, "SLICE3")},
            {STORE_SLICE, new OpCode(STORE_SLICE, "STORE_SLICE")},
            {STORE_SLICE1, new OpCode(STORE_SLICE1, "STORE_SLICE1")},
            {STORE_SLICE2, new OpCode(STORE_SLICE2, "STORE_SLICE2")},
            {STORE_SLICE3, new OpCode(STORE_SLICE3, "STORE_SLICE3")},
            {DELETE_SLICE, new OpCode(DELETE_SLICE, "DELETE_SLICE")},
            {DELETE_SLICE1, new OpCode(DELETE_SLICE1, "DELETE_SLICE1")},
            {DELETE_SLICE2, new OpCode(DELETE_SLICE2, "DELETE_SLICE2")},
            {DELETE_SLICE3, new OpCode(DELETE_SLICE3, "DELETE_SLICE3")},
            {STORE_MAP, new OpCode(STORE_MAP, "STORE_MAP")},
            {INPLACE_ADD, new OpCode(INPLACE_ADD, "INPLACE_ADD")},
            {INPLACE_SUBTRACT, new OpCode(INPLACE_SUBTRACT, "INPLACE_SUBTRACT")},
            {INPLACE_MULTIPLY, new OpCode(INPLACE_MULTIPLY, "INPLACE_MULTIPLY")},
            {INPLACE_DIVIDE, new OpCode(INPLACE_DIVIDE, "INPLACE_DIVIDE")},
            {INPLACE_MODULO, new OpCode(INPLACE_MODULO, "INPLACE_MODULO")},
            {STORE_SUBSCR, new OpCode(STORE_SUBSCR, "STORE_SUBSCR")},
            {DELETE_SUBSCR, new OpCode(DELETE_SUBSCR, "DELETE_SUBSCR")},
            {BINARY_LSHIFT, new OpCode(BINARY_LSHIFT, "BINARY_LSHIFT")},
            {BINARY_RSHIFT, new OpCode(BINARY_RSHIFT, "BINARY_RSHIFT")},
            {BINARY_AND, new OpCode(BINARY_AND, "BINARY_AND")},
            {BINARY_XOR, new OpCode(BINARY_XOR, "BINARY_XOR")},
            {BINARY_OR, new OpCode(BINARY_OR, "BINARY_OR")},
            {INPLACE_POWER, new OpCode(INPLACE_POWER, "INPLACE_POWER")},
            {GET_ITER, new OpCode(GET_ITER, "GET_ITER")},
            {PRINT_EXPR, new OpCode(PRINT_EXPR, "PRINT_EXPR")},
            {PRINT_ITEM, new OpCode(PRINT_ITEM, "PRINT_ITEM")},
            {PRINT_NEWLINE, new OpCode(PRINT_NEWLINE, "PRINT_NEWLINE")},
            {PRINT_ITEM_TO, new OpCode(PRINT_ITEM_TO, "PRINT_ITEM_TO")},
            {PRINT_NEWLINE_TO, new OpCode(PRINT_NEWLINE_TO, "PRINT_NEWLINE_TO")},
            {INPLACE_LSHIFT, new OpCode(INPLACE_LSHIFT, "INPLACE_LSHIFT")},
            {INPLACE_RSHIFT, new OpCode(INPLACE_RSHIFT, "INPLACE_RSHIFT")},
            {INPLACE_AND, new OpCode(INPLACE_AND, "INPLACE_AND")},
            {INPLACE_XOR, new OpCode(INPLACE_XOR, "INPLACE_XOR")},
            {INPLACE_OR, new OpCode(INPLACE_OR, "INPLACE_OR")},
            {BREAK_LOOP, new OpCode(BREAK_LOOP, "BREAK_LOOP")},
            {WITH_CLEANUP, new OpCode(WITH_CLEANUP, "WITH_CLEANUP")},
            {LOAD_LOCALS, new OpCode(LOAD_LOCALS, "LOAD_LOCALS")},
            {RETURN_VALUE, new OpCode(RETURN_VALUE, "RETURN_VALUE")},
            {IMPORT_STAR, new OpCode(IMPORT_STAR, "IMPORT_STAR")},
            {EXEC_STMT, new OpCode(EXEC_STMT, "EXEC_STMT")},
            {YIELD_VALUE, new OpCode(YIELD_VALUE, "YIELD_VALUE")},
            {POP_BLOCK, new OpCode(POP_BLOCK, "POP_BLOCK")},
            {END_FINALLY, new OpCode(END_FINALLY, "END_FINALLY")},
            {BUILD_CLASS, new OpCode(BUILD_CLASS, "BUILD_CLASS")},
            {STORE_NAME, new OpCode(STORE_NAME, "STORE_NAME"){ HasArgument = true, HasName = true}},
            {DELETE_NAME, new OpCode(DELETE_NAME, "DELETE_NAME"){ HasArgument = true, HasName = true}},
            {UNPACK_SEQUENCE, new OpCode(UNPACK_SEQUENCE, "UNPACK_SEQUENCE"){ HasArgument = true}},
            {FOR_ITER, new OpCode(FOR_ITER, "FOR_ITER"){ HasArgument = true, HasJumpRelative = true}},
            {LIST_APPEND, new OpCode(LIST_APPEND, "LIST_APPEND"){ HasArgument = true}},
            {STORE_ATTR, new OpCode(STORE_ATTR, "STORE_ATTR"){ HasArgument = true, HasName = true}},
            {DELETE_ATTR, new OpCode(DELETE_ATTR, "DELETE_ATTR"){ HasArgument = true, HasName = true}},
            {STORE_GLOBAL, new OpCode(STORE_GLOBAL, "STORE_GLOBAL"){ HasArgument = true, HasName = true}},
            {DELETE_GLOBAL, new OpCode(DELETE_GLOBAL, "DELETE_GLOBAL"){ HasArgument = true, HasName = true}},
            {DUP_TOPX, new OpCode(DUP_TOPX, "DUP_TOPX"){ HasArgument = true}},
            {LOAD_CONST, new OpCode(LOAD_CONST, "LOAD_CONST"){ HasArgument = true, HasConstant = true}},
            {LOAD_NAME, new OpCode(LOAD_NAME, "LOAD_NAME"){ HasArgument = true, HasName = true}},
            {BUILD_TUPLE, new OpCode(BUILD_TUPLE, "BUILD_TUPLE"){ HasArgument = true}},
            {BUILD_LIST, new OpCode(BUILD_LIST, "BUILD_LIST"){ HasArgument = true}},
            {BUILD_SET, new OpCode(BUILD_SET, "BUILD_SET"){ HasArgument = true}},
            {BUILD_MAP, new OpCode(BUILD_MAP, "BUILD_MAP"){ HasArgument = true}},
            {LOAD_ATTR, new OpCode(LOAD_ATTR, "LOAD_ATTR"){ HasArgument = true, HasName = true}},
            {COMPARE_OP, new OpCode(COMPARE_OP, "COMPARE_OP"){ HasArgument = true, HasCompare = true}},
            {IMPORT_NAME, new OpCode(IMPORT_NAME, "IMPORT_NAME"){ HasArgument = true, HasName = true}},
            {IMPORT_FROM, new OpCode(IMPORT_FROM, "IMPORT_FROM"){ HasArgument = true, HasName = true}},
            {JUMP_FORWARD, new OpCode(JUMP_FORWARD, "JUMP_FORWARD"){ HasArgument = true, HasJumpRelative = true}},
            {JUMP_IF_FALSE_OR_POP, new OpCode(JUMP_IF_FALSE_OR_POP, "JUMP_IF_FALSE_OR_POP"){ HasArgument = true, HasJumpAbsolute = true}},
            {JUMP_IF_TRUE_OR_POP, new OpCode(JUMP_IF_TRUE_OR_POP, "JUMP_IF_TRUE_OR_POP"){ HasArgument = true, HasJumpAbsolute = true}},
            {JUMP_ABSOLUTE, new OpCode(JUMP_ABSOLUTE, "JUMP_ABSOLUTE"){ HasArgument = true, HasJumpAbsolute = true}},
            {POP_JUMP_IF_FALSE, new OpCode(POP_JUMP_IF_FALSE, "POP_JUMP_IF_FALSE"){ HasArgument = true, HasJumpAbsolute = true}},
            {POP_JUMP_IF_TRUE, new OpCode(POP_JUMP_IF_TRUE, "POP_JUMP_IF_TRUE"){ HasArgument = true, HasJumpAbsolute = true}},
            {LOAD_GLOBAL, new OpCode(LOAD_GLOBAL, "LOAD_GLOBAL"){ HasArgument = true, HasName = true}},
            {CONTINUE_LOOP, new OpCode(CONTINUE_LOOP, "CONTINUE_LOOP"){ HasArgument = true, HasJumpAbsolute = true}},
            {SETUP_LOOP, new OpCode(SETUP_LOOP, "SETUP_LOOP"){ HasArgument = true, HasJumpRelative = true}},
            {SETUP_EXCEPT, new OpCode(SETUP_EXCEPT, "SETUP_EXCEPT"){ HasArgument = true, HasJumpRelative = true}},
            {SETUP_FINALLY, new OpCode(SETUP_FINALLY, "SETUP_FINALLY"){ HasArgument = true, HasJumpRelative = true}},
            {LOAD_FAST, new OpCode(LOAD_FAST, "LOAD_FAST"){ HasArgument = true, HasLocal = true}},
            {STORE_FAST, new OpCode(STORE_FAST, "STORE_FAST"){ HasArgument = true, HasLocal = true}},
            {DELETE_FAST, new OpCode(DELETE_FAST, "DELETE_FAST"){ HasArgument = true, HasLocal = true}},
            {RAISE_VARARGS, new OpCode(RAISE_VARARGS, "RAISE_VARARGS"){ HasArgument = true}},
            {CALL_FUNCTION, new OpCode(CALL_FUNCTION, "CALL_FUNCTION"){ HasArgument = true}},
            {MAKE_FUNCTION, new OpCode(MAKE_FUNCTION, "MAKE_FUNCTION"){ HasArgument = true}},
            {BUILD_SLICE, new OpCode(BUILD_SLICE, "BUILD_SLICE"){ HasArgument = true}},
            {MAKE_CLOSURE, new OpCode(MAKE_CLOSURE, "MAKE_CLOSURE"){ HasArgument = true}},
            {LOAD_CLOSURE, new OpCode(LOAD_CLOSURE, "LOAD_CLOSURE"){ HasArgument = true, HasFree = true}},
            {LOAD_DEREF, new OpCode(LOAD_DEREF, "LOAD_DEREF"){ HasArgument = true, HasFree = true}},
            {STORE_DEREF, new OpCode(STORE_DEREF, "STORE_DEREF"){ HasArgument = true, HasFree = true}},
            {CALL_FUNCTION_VAR, new OpCode(CALL_FUNCTION_VAR, "CALL_FUNCTION_VAR"){ HasArgument = true}},
            {CALL_FUNCTION_KW, new OpCode(CALL_FUNCTION_KW, "CALL_FUNCTION_KW"){ HasArgument = true}},
            {CALL_FUNCTION_VAR_KW, new OpCode(CALL_FUNCTION_VAR_KW, "CALL_FUNCTION_VAR_KW"){ HasArgument = true}},
            {SETUP_WITH, new OpCode(SETUP_WITH, "SETUP_WITH"){ HasArgument = true, HasJumpRelative = true}},
            {EXTENDED_ARG, new OpCode(EXTENDED_ARG, "EXTENDED_ARG"){ HasArgument = true}},
            {SET_ADD, new OpCode(SET_ADD, "SET_ADD"){ HasArgument = true}},
            {MAP_ADD, new OpCode(MAP_ADD, "MAP_ADD"){ HasArgument = true}}
        };

        public static List<string> CompareOpNames = new List<string> { "<", "<=", "==", "!=", ">", ">=", "in", "not in", "is", "is not", "exception match", "BAD" };

        public List<OpCode> Instructions = new List<OpCode>();
        public int CurrentInstructionIndex = -1;
        public bool HasInstructionsToProcess { get { return CurrentInstructionIndex < Instructions.Count - 1; } }
        public PythonCodeObject CodeObject { get; set; }

        public int CurrentOffset { get { return CurrentInstructionIndex < 0 ? 0 : Instructions[CurrentInstructionIndex].Offset; } }
        public OpCode Current { get { return CurrentInstructionIndex < 0 ? null : Instructions[CurrentInstructionIndex]; } }

        public OpCodes(PythonCodeObject co)
        {
            CodeObject = co;
            int idx = 0;
            byte opCodeID = 0;
            uint extendedArg = 0;
            while (idx < CodeObject.Code.Length)
            {
                opCodeID = CodeObject.Code[idx++];
                OpCode opCode = OpCodeList[opCodeID].Clone();
                opCode.Offset = idx - 1;

                if (opCode.HasArgument)
                {
                    opCode.Argument = ((uint)CodeObject.Code[idx++]) + ((uint)CodeObject.Code[idx++]) * 256 + extendedArg;
                    extendedArg = 0;
                    if (opCodeID == OpCodes.EXTENDED_ARG)
                        extendedArg = opCode.Argument * 65536;
                    if (opCode.HasConstant)
                    {
                        PythonObject val = CodeObject.Consts[(int)opCode.Argument];
                        opCode.Constant = val.ClassName == "Py_CodeObject" ? opCode.Argument.ToString() : val.ClassName == "Py_String" ? "'" + (string)val + "'" : (string)val;
                    }
                    else if (opCode.HasName)
                    {
                        opCode.Name = CodeObject.Names[(int)opCode.Argument];
                    }
                    else if (opCode.HasCompare)
                    {
                        opCode.CompareOperator = OpCodes.CompareOpNames[(int)opCode.Argument];
                    }
                    else if (opCode.HasLocal)
                    {
                        opCode.LocalName = CodeObject.VarNames[(int)opCode.Argument];
                    }
                    else if (opCode.HasFree)
                    {
                        if ((int)opCode.Argument < CodeObject.CellVars.Count)
                        {
                            opCode.FreeName = CodeObject.CellVars[(int)opCode.Argument];
                        }
                        else if (((int)opCode.Argument - CodeObject.CellVars.Count) < CodeObject.FreeVars.Count)
                        {
                            opCode.FreeName = CodeObject.FreeVars[(int)opCode.Argument - CodeObject.CellVars.Count];
                        }
                        else
                        {
                            throw new ArgumentException("ERROR: Cannot calculate Free Var for index " + (int)opCode.Argument);
                        }
                    }
                }
                Instructions.Add(opCode);
            }
        }

        public OpCode GetNextInstruction()
        {
            CurrentInstructionIndex++;
            if (CurrentInstructionIndex >= Instructions.Count)
                return null;
            return Instructions[CurrentInstructionIndex];
        }

        public OpCode PeekPrevInstruction(int n = 1)
        {
            if (CurrentInstructionIndex - n < 0)
                return null;
            return Instructions[CurrentInstructionIndex - n];
        }

        public OpCode PeekNextInstruction(int n = 1)
        {
            if (CurrentInstructionIndex + n >= Instructions.Count)
                return null;
            return Instructions[CurrentInstructionIndex + n];
        }

        public OpCode PeekInstructionAt(int n)
        {
            if (n >= Instructions.Count || n < 0)
                return null;
            return Instructions[n];
        }

        public OpCode PeekInstructionAtOffset(uint off)
        {
            int startPos = 0;
            if (off > Instructions[CurrentInstructionIndex].Offset)
                startPos = CurrentInstructionIndex + 1;

            for (int idx = startPos; idx < Instructions.Count; idx++)
            {
                if (Instructions[idx].Offset == off)
                    return Instructions[idx];
            }

            return null;
        }

        public OpCode PeekInstructionBeforeOffset(uint off, int n = 1)
        {
            int idx = GetIndexByOffset((int)off);

            return PeekInstructionAt(idx - n);
        }

        public int GetIndexByOffset(int offset)
        {
            for (int idx = 0; idx < Instructions.Count; idx++)
            {
                if (Instructions[idx].Offset == offset)
                    return idx;
            }

            return -1;
        }
        public int GetIndexByOpCode(int opCodeID)
        {
            for (int idx = CurrentInstructionIndex+1; idx < Instructions.Count; idx++)
            {
                if (Instructions[idx].OpCodeID == opCodeID)
                    return idx;
            }

            return -1;
        }

        public int GetOffsetByOpCode(int opCodeID)
        {
            for (int idx = CurrentInstructionIndex + 1; idx < Instructions.Count; idx++)
            {
                if (Instructions[idx].OpCodeID == opCodeID)
                    return Instructions[idx].Offset;
            }

            return -1;
        }

        public int GetReversedOffsetByOpCode(int opCodeID, int startOffset = -1, int endOffset = -1)
        {
            int startIndex = CurrentInstructionIndex - 1;
            int endIndex = 0;

            if (startOffset > 0)
                startIndex = GetIndexByOffset(startOffset);

            if (endOffset > 0)
                endIndex = GetIndexByOffset(endOffset);

            for (int idx = startIndex; idx >= endIndex; idx--)
            {
                if (Instructions[idx].OpCodeID == opCodeID)
                    return Instructions[idx].Offset;
            }

            return -1;
        }

        public void SkipInstruction(int n = 1)
        {
            CurrentInstructionIndex += n;
        }

        public int GetOffsetByOpCodeName(string opName)
        {
            for (int idx = CurrentInstructionIndex + 1; idx < Instructions.Count; idx++)
            {
                if (Instructions[idx].InstructionName.StartsWith(opName))
                    return Instructions[idx].Offset;
            }

            return -1;
        }

        public int GetBackOffsetByOpCodeName(string opName)
        {
            for (int idx = CurrentInstructionIndex - 1; idx >= 0; idx--)
            {
                if (Instructions[idx].InstructionName.StartsWith(opName))
                    return Instructions[idx].Offset;
            }

            return -1;
        }

        public int GetLineOffsetRangeForOffset(uint offset, out uint startOffset, out uint endOffset)
        {
            startOffset = (uint)GetIndexByOffset((int)offset);
            endOffset = startOffset;
            int line = CodeObject.LineNoTab[(int)offset];

            while (startOffset > 0)
            {
                startOffset--;
                if (CodeObject.LineNoTab[Instructions[(int)startOffset].Offset] != line)
                {
                    startOffset++;
                    break;
                }
            }

            while (endOffset < CodeObject.Code.Length)
            {
                endOffset++;
                if (CodeObject.LineNoTab[Instructions[(int)endOffset].Offset] != line)
                {
                    endOffset--;
                    break;
                }
            }

            startOffset = (uint)Instructions[(int)startOffset].Offset;
            endOffset = (uint)Instructions[(int)endOffset].Offset;
            return line;
        }

        public int CountSpecificOpCodes(string opCode, int off = -1)
        {
            int startPos = 0;
            int count = 0;
            if (off > Instructions[CurrentInstructionIndex].Offset)
                startPos = CurrentInstructionIndex + 1;

            for (int idx = startPos; idx < Instructions.Count; idx++)
            {
                if (off > 0 && Instructions[idx].Offset >= off)
                    break;
                if (Instructions[idx].InstructionName.StartsWith(opCode))
                    count++;
            }

            return count;
        }

        public bool CheckIfOpCodesExistsInLine(List<byte> opCodes, uint sOff, uint eOff)
        {
            int startPos = GetIndexByOffset((int)sOff);
            int endPos = GetIndexByOffset((int)eOff);
            Dictionary<byte,bool> mapExists = new Dictionary<byte,bool>();

            foreach (byte opCode in opCodes)
	        {
		        mapExists[opCode] = false;
            }

            for (int idx = startPos; idx <= endPos; idx++)
            {
                if (opCodes.Contains(Instructions[idx].OpCodeID))
                {
                    mapExists[Instructions[idx].OpCodeID] = true;
                }
            }

            return mapExists.All(pair => pair.Value);
        }

    }
}
